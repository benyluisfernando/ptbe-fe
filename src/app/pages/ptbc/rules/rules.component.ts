import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BackendService } from 'src/app/services/backend.service';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/root/organization.service';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent implements OnInit {
  display = false;
  selectedOrganization: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  userInfo:any = {};
  tokenID:string = "";
  orgheader:any = [
                  {'label':'Rule ID', 'sort':'ruleid'},
                  {'label':'Channel ID', 'sort':'channelid'},
                  {'label':'Company ID', 'sort':'companyid'},
                  {'label':'Rule No', 'sort':'ruleno'},
                  {'label':'Rule Type', 'sort':'ruletype'},
                  {'label':'Create Who', 'sort':'createdwho'},
                  {'label':'Create Date', 'sort':'createddate'}];
  orgcolname:any = ["RULE_ID", "CHANNEL_ID","COMPANY_ID","RULE_NO","RULE_TYPE","CREATE_WHO","CREATE_DATE"]
  orgcolhalign:any = ["p-text-center", "","","p-text-center","p-text-center"]
  orgcolwidth:any = [{'width':'110px'},"","",{'width':'120px'},""];
  orgactionbtn:any = [1,1,1,1,1]
  orgaddbtn ={'route':'detail', 'label':'Add Data'}
  ruleslist: any[] = [];


  constructor(private authservice:AuthService,public dialogService: DialogService,
    public messageService: MessageService,
    private backend: BackendService,
    private organizationService: OrganizationService) { }
  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.breadcrumbs = [
      { label: 'Rules' }
    ];
    // this.authservice.whoAmi().subscribe((value) =>{
    //   // console.log(">>> User Info : "+JSON.stringify(value));
    //     this.userInfo = value.data;
    //     this.tokenID = value.tokenId;
    // });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    this.backend
      .post(
       // 'restv2/billpayment.services.portal.ws:getCompany/getCompany',
         'api/rules/getRules',
        {}
        ,
        false
      ).subscribe((data: BackendResponse) => {
        //console.log("Company length>>>>>>> " + JSON.stringify(data));
        // if ((data.status = 200)) {
        //   this.organizationService
        //     .retriveOrgByTenant()
        //     .subscribe((orgall: BackendResponse) => {
        //console.log('>>>>>>> ' + data.data.data.length);
        this.ruleslist = data.data.data;

        if (this.ruleslist.length < 1) {
          let objtmp = { "orgcode": "No records", "orgname": "No records", "orgdescription": "No records", "application": "No records", "created_by": "No records" }
          this.ruleslist = [];
          this.ruleslist.push(objtmp);
        }



        this.isFetching = false;
        //     });
        // }
      });
  }
  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.display = true;
    this.selectedOrganization = data;
  }
  deleteOrganization() {
    console.log(this.selectedOrganization);
    let organization = this.selectedOrganization;
    const payload = { organization };
    this.organizationService
      .deleteOrg(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingApp();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
