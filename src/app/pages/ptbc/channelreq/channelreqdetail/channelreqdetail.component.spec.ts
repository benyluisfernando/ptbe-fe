import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelreqdetailComponent } from './channelreqdetail.component';

describe('ChannelreqdetailComponent', () => {
  let component: ChannelreqdetailComponent;
  let fixture: ComponentFixture<ChannelreqdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelreqdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelreqdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
