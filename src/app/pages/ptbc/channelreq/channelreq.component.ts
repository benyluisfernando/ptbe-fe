import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BackendService } from 'src/app/services/backend.service';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/root/organization.service';

@Component({
  selector: 'app-channelreq',
  templateUrl: './channelreq.component.html',
  styleUrls: ['./channelreq.component.scss']
})
export class ChannelreqComponent implements OnInit {
  viewApprove = false;
  viewDisplay = false;
  display = false;
  selectedOrganization: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = "";

  orgheader:any = [{'label':'Channel ID', 'sort':'id'},
  {'label':'Merchant Type', 'sort':'channeltype'},
  {'label':'Channel Name', 'sort':'channelname'},
  {'label':'Created Who', 'sort':'createdwho'},
  {'label':'Created Date', 'sort':'createddate'},
  {'label':'Change Who', 'sort':'changedwho'},
  {'label':'Change Date', 'sort':'changeddate'},
  {'label':'Channel Code', 'sort':'channelcode'},
  {'label':'ID', 'sort':'id'}];

  orgcolname:any = ["CHANNEL_ID","MERCHANT_TYPE","CHANNEL_NAME","CREATE_WHO","CREATE_DATE","CHANGE_WHO","CHANGE_DATE","CHANNEL_CODE","ID"]
  orgcolhalign: any = ["p-text-center", "", "", "p-text-center", "p-text-center", ""]
  orgcolwidth: any = [{ 'width': '110px' }, "", "", { 'width': '120px' }, ""];
  orgactionbtn: any = [1, 1, 1, 1, 1]
  orgaddbtn = { 'route': 'detail', 'label': 'Add Data' }
  channelreqlist: any[] = [];


  constructor(private authservice:AuthService,public dialogService: DialogService,
    public messageService: MessageService,
    private backend: BackendService,
    private organizationService: OrganizationService) { }
  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.breadcrumbs = [
      { label: 'Pending Company' }
    ];
    // this.authservice.whoAmi().subscribe((value) =>{
    //   // console.log(">>> User Info : "+JSON.stringify(value));
    //     this.userInfo = value.data;
    //     this.tokenID = value.tokenId;
    // });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    this.backend
      .post(
       // 'restv2/billpayment.services.portal.ws:getCompany/getCompany',
         'api/channelreq/getChannelTemp',
        {}
        ,
        false
      ).subscribe((data: BackendResponse) => {
        //console.log("Company length>>>>>>> " + JSON.stringify(data));
        // if ((data.status = 200)) {
        //   this.organizationService
        //     .retriveOrgByTenant()
        //     .subscribe((orgall: BackendResponse) => {
        //console.log('>>>>>>> ' + data.data.data.length);
        this.channelreqlist = data.data.data;

        if (this.channelreqlist.length < 1) {
          let objtmp = { "orgcode": "No records", "orgname": "No records", "orgdescription": "No records", "application": "No records", "created_by": "No records" }
          this.channelreqlist = [];
          this.channelreqlist.push(objtmp);
        }



        this.isFetching = false;
        //     });
        // }
      });
  }
  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.display = true;
    this.selectedOrganization = data;
  }
  deleteOrganization() {
    console.log(this.selectedOrganization);
    let organization = this.selectedOrganization;
    const payload = { organization };
    this.organizationService
      .deleteOrg(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingApp();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
