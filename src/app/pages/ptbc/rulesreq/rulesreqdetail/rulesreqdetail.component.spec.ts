import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RulesreqdetailComponent } from './rulesreqdetail.component';

describe('RulesreqdetailComponent', () => {
  let component: RulesreqdetailComponent;
  let fixture: ComponentFixture<RulesreqdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RulesreqdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RulesreqdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
