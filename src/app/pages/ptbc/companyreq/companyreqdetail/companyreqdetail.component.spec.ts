import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyreqdetailComponent } from './companyreqdetail.component';

describe('CompanyreqdetailComponent', () => {
  let component: CompanyreqdetailComponent;
  let fixture: ComponentFixture<CompanyreqdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyreqdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyreqdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
