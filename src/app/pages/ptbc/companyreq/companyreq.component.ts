import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BackendService } from 'src/app/services/backend.service';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/root/organization.service';

@Component({
  selector: 'app-companyreq',
  templateUrl: './companyreq.component.html',
  styleUrls: ['./companyreq.component.scss']
})
export class CompanyreqComponent implements OnInit {
  display = false;
  selectedOrganization: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  userInfo:any = {};
  tokenID:string = "";
  orgheader:any = [{'label':'Company ID', 'sort':'companyid'},
                  {'label':'Company Code', 'sort':'companycode'},
                  {'label':'CIF Code', 'sort':'companyname'},
                  {'label':'Company Name', 'sort':'hostcode'},
                  {'label':'Company Short Name', 'sort':'createdwho'}];
  orgcolname:any = ["COMPANY_ID", "COMPANY_CODE","CIF_CODE","COMPANY_NAME","COMPANY_SHORTNAME"]
  orgcolhalign:any = ["p-text-center", "","","p-text-center","p-text-center"]
  orgcolwidth:any = [{'width':'110px'},"","",{'width':'120px'},""];
  orgactionbtn:any = [1,1,1,1,1]
  orgaddbtn ={'route':'detail', 'label':'Add Data'}
  companyreqlist: any[] = [];


  constructor(private authservice:AuthService,public dialogService: DialogService,
    public messageService: MessageService,
    private backend: BackendService,
    private organizationService: OrganizationService) { }
  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/'};
    this.breadcrumbs = [
      { label: 'Pending Company' }
    ];
    // this.authservice.whoAmi().subscribe((value) =>{
    //   // console.log(">>> User Info : "+JSON.stringify(value));
    //     this.userInfo = value.data;
    //     this.tokenID = value.tokenId;
    // });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    this.backend
      .post(
       // 'restv2/billpayment.services.portal.ws:getCompany/getCompany',
         'api/companyreq/getCompanyTemp',
        {}
        ,
        false
      ).subscribe((data: BackendResponse) => {
        //console.log("Company length>>>>>>> " + JSON.stringify(data));
        // if ((data.status = 200)) {
        //   this.organizationService
        //     .retriveOrgByTenant()
        //     .subscribe((orgall: BackendResponse) => {
        //console.log('>>>>>>> ' + data.data.data.length);
        this.companyreqlist = data.data.data;

        if (this.companyreqlist.length < 1) {
          let objtmp = { "orgcode": "No records", "orgname": "No records", "orgdescription": "No records", "application": "No records", "created_by": "No records" }
          this.companyreqlist = [];
          this.companyreqlist.push(objtmp);
        }



        this.isFetching = false;
        //     });
        // }
      });
  }
  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.display = true;
    this.selectedOrganization = data;
  }
  deleteOrganization() {
    console.log(this.selectedOrganization);
    let organization = this.selectedOrganization;
    const payload = { organization };
    this.organizationService
      .deleteOrg(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingApp();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
