import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyreqComponent } from './companyreq.component';

describe('CompanyreqComponent', () => {
  let component: CompanyreqComponent;
  let fixture: ComponentFixture<CompanyreqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyreqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyreqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
