import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BackendService } from 'src/app/services/backend.service';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/root/organization.service';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})
 

export class ChannelComponent implements OnInit {
  viewApprove = false;
  viewDisplay = false;
  display = false;
  selectedOrganization: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = "";
  orgheader: any = [{ 'label': 'CHANNEL_ID', 'sort': 'CHANNEL_ID' },                  
                    { 'label': 'CHANNEL_NAME', 'sort': 'CHANNEL_NAME' },                    
                    { 'label': 'CHANNEL_CODE', 'sort': 'CHANNEL_CODE' },
                    { 'label': 'MERCHANT_TYPE', 'sort': 'MERCHANT_TYPE' },
                    { 'label': 'CREATE_WHO', 'sort': 'CREATE_WHO' },
                    { 'label': 'CREATE_DATE', 'sort': 'CREATE_DATE' },
                    { 'label': 'CHANGE_WHO', 'sort': 'CHANGE_WHO' },
                    { 'label': 'CHANGE_DATE', 'sort': 'CHANGE_DATE' }];


  orgcolname: any = ["CHANNEL_ID", "CHANNEL_NAME", "CHANNEL_CODE", "MERCHANT_TYPE", "CREATE_WHO", "CREATE_DATE","CHANGE_WHO","CHANGE_DATE"]
  orgcolhalign: any = ["p-text-center", "", "", "p-text-center", "p-text-center", ""]
  orgcolwidth: any = [{ 'width': '110px' }, "", "", { 'width': '120px' }, ""];
  orgactionbtn: any = [1, 1, 1, 1, 1,1, 1, 1]
  orgaddbtn = { 'route': 'detail', 'label': 'Add Data' }
  channellist: any[] = [];


  constructor(private authservice: AuthService, public dialogService: DialogService,
    public messageService: MessageService,
    private backend: BackendService,
    private organizationService: OrganizationService) { }
  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/' };
    this.breadcrumbs = [
      { label: 'Channel' }
    ];
    // this.authservice.whoAmi().subscribe((value) =>{
    //   // console.log(">>> User Info : "+JSON.stringify(value));
    //     this.userInfo = value.data;
    //     this.tokenID = value.tokenId;
    // });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    this.backend
      .post(
        'api/channel/getChannel',
        {}
        ,
        false
      ).subscribe((data: BackendResponse) => {
        console.log("Company length>>>>>>> " + JSON.stringify(data));
        // if ((data.status = 200)) {
        //   this.organizationService
        //     .retriveOrgByTenant()
        //     .subscribe((orgall: BackendResponse) => {
        // console.log('>>>>>>> ' + JSON.stringify(orgall));
        this.channellist = data.data.data;
        if (this.channellist.length < 1) {
          let objtmp = { "CHANNEL_ID": "No records", 
                         "CHANNEL_NAME": "No records", 
                         "CHANNEL_CODE": "No records", 
                         "MERCHANT_TYPE": "No records", 
                         "CREATE_WHO": "No records", 
                         "CREATE_DATE": "No records", 
                         "CHANGE_WHO": "No records", 
                         "CHANGE_DATE": "No records" }
          this.channellist = [];
          this.channellist.push(objtmp);
        }



        this.isFetching = false;
        //     });
        // }
      });
  }

  viewData(data: any) {
    console.log(data);
    this.viewDisplay = true;
    this.selectedOrganization = data;
  }

  approvalData(data: any) {
    // console.log(data);
    this.viewApprove = true;
    this.selectedOrganization = data;

  }
  approvalSubmit(status){
    // console.log(this.selectedCompany);
    // console.log(status);
    let payload = {
      id: this.selectedOrganization.id,
      oldactive:this.selectedOrganization.active,
      isactive: status,
      idapproval: this.selectedOrganization.idapproval
    };
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.organizationService
      .updatebyAdminActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingApp();
          this.viewApprove = false;
        }
      });
  }

  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child " + JSON.stringify(data));
    this.display = true;
    this.selectedOrganization = data;
  }
  deleteOrganization() {
    console.log(this.selectedOrganization);
    let organization = this.selectedOrganization;
    const payload = { organization };
    this.organizationService
      .deleteOrg(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingApp();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
