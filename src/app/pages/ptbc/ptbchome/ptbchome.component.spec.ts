import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PtbchomeComponent } from './ptbchome.component';

describe('PtbchomeComponent', () => {
  let component: PtbchomeComponent;
  let fixture: ComponentFixture<PtbchomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PtbchomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PtbchomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
