import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BackendService } from 'src/app/services/backend.service';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/root/organization.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  viewDisplay = false;
  display = false;
  selectedCompany: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  companyInfo:any = {};
  tokenID:string = "";
  orgheader:any = [{'label':'Company ID', 'sort':'companyid'},
  {'label':'Company Code', 'sort':'companycode'},
  {'label':'Company Name', 'sort':'companyname'},
  {'label':'Host Code', 'sort':'hostcode'},
  {'label':'Created Who', 'sort':'createdwho'},
  {'label':'Created Date', 'sort':'createddate'}];
  orgcolname:any = ["COMPANY_ID", "COMPANY_CODE","COMPANY_NAME","HOST_CODE","CREATE_WHO","CREATE_DATE"]
  orgcolhalign:any = ["p-text-center", "","","p-text-center","p-text-center"]
  orgcolwidth:any = [{'width':'110px'},"","",{'width':'120px'},""];
  orgactionbtn:any = [1,1,1,1,1]
  orgaddbtn ={'route':'detail', 'label':'Add Data'}
  companylist: any[] = [];
  router: any;

  constructor(private authservice: AuthService, public dialogService: DialogService,
    public messageService: MessageService,
    private aclMenuservice : AclmenucheckerService,
    private backend: BackendService,
    private organizationService: OrganizationService) { }
  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/' };
    this.breadcrumbs = [
      { label: 'Company List' }
    ];

    this.aclMenuservice.setAllMenus(this.companyInfo.sidemenus).then(data=>{
      console.log("MENU ALL ACL SET");
      this.aclMenuservice.getAclMenu(this.router.url).then(dataacl =>{
        if(JSON.stringify(dataacl.acl) === "{}"){
          console.log("No ACL Founded")
        }else{
          console.log("ACL Founded");
          console.log(dataacl.acl);
          this.orgactionbtn[0] = dataacl.acl.create;
          this.orgactionbtn[1] = dataacl.acl.read;
          this.orgactionbtn[2] = dataacl.acl.update;
          this.orgactionbtn[3] = dataacl.acl.delete;
          this.orgactionbtn[4] = dataacl.acl.view;
          this.orgactionbtn[5] = dataacl.acl.approval;
        }
      });
    });
    // this.authservice.whoAmi().subscribe((value) =>{
    //   // console.log(">>> User Info : "+JSON.stringify(value));
    //     this.userInfo = value.data;
    //     this.tokenID = value.tokenId;
    // });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    this.backend
      .post(
       // 'restv2/billpayment.services.portal.ws:getCompany/getCompany',
         'api/company/getCompany',
        {}
        ,
        false
      ).subscribe((data: BackendResponse) => {
        //console.log("Company length>>>>>>> " + JSON.stringify(data));
        // if ((data.status = 200)) {
        //   this.organizationService
        //     .retriveOrgByTenant()
        //     .subscribe((orgall: BackendResponse) => {
        //console.log('>>>>>>> ' + data.data.data.length);
        this.companylist = data.data.data;

        if (this.companylist.length < 1) {
          let objtmp = { "orgcode": "No records", "orgname": "No records", "orgdescription": "No records", "application": "No records", "created_by": "No records" }
          this.companylist = [];
          this.companylist.push(objtmp);
        }



        this.isFetching = false;
        //     });
        // }
      });
  }

  viewData(data: any) {
    console.log('di ambil dari child'+ JSON.stringify(data));
    this.viewDisplay = true;
    this.selectedCompany = data;
  }

  // approvalData(data: any) {
  //   // console.log(data);
  //   this.viewApprove = true;
  //   this.selectedOrganization = data;

  // }
  approvalSubmit(status){
    // console.log(this.selectedCompany);
    // console.log(status);
    let payload = {
      id: this.selectedCompany.id,
      oldactive:this.selectedCompany.active,
      isactive: status,
      idapproval: this.selectedCompany.idapproval
    };
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.organizationService
      .updatebyAdminActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingApp();
          // this.viewApprove = false;
        }
      });
  }

  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child " + JSON.stringify(data));
    this.display = true;
    this.selectedCompany = data;
  }
  deleteOrganization() {
    console.log(this.selectedCompany);
    let organization = this.selectedCompany;
    const payload = { organization };
    this.organizationService
      .deleteOrg(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingApp();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
