import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { UsermanagerService } from 'src/app/services/root/usermanager.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  display = false;
  scrollheight:any ="400px"
  viewDisplay = false;
  viewApprove = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  userlist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  usrheader: any = [
    { label: 'Name', sort: 'fullname' },
    { label: 'User ID', sort: 'userid' },
    { label: 'Status', sort: 'active' },
    { label: 'Group Menu', sort: 'groupname' },
    { label: 'Attempt', sort: 'total_attempt' },
    { label: 'Created At', sort: 'created_at' },
  ];
  usrcolname: any = [
    'fullname',
    'userid',
    'active',
    'groupname',
    'total_attempt',
    'created_at',
  ];
  usrcolhalign: any = [
    '',
    '',
    'p-text-center',
    '',
    'p-text-center',
    'p-text-center',
  ];
  // usrcolwidth: any = [
  //   '',
  //   '',
  //   { width: '110px' },
  //   { width: '170px' },
  //   { width: '110px' },
  //   { width: '170px' },
  // ];
  usrcolwidth: any = [
    '',
    '',
    '',
    '',
    { width: '170px' },
    '',
  ];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [1, 1, 1, 1, 1, 1];
  usraddbtn = { route: 'detail', label: 'Add Data' };

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private userService: UsermanagerService,
    private aclMenuService : AclmenucheckerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Users Management' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then(dataacl =>{
          if(JSON.stringify(dataacl.acl) === "{}"){
            console.log("No ACL Founded")
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);
            this.usractionbtn[0] = dataacl.acl.create;
              this.usractionbtn[1] = dataacl.acl.read;
              this.usractionbtn[2] = dataacl.acl.update;
              this.usractionbtn[3] = dataacl.acl.delete;
              this.usractionbtn[4] = dataacl.acl.view;
              this.usractionbtn[5] = dataacl.acl.approval;
          }
        });
      });

    });
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      if ((data.status = 200)) {
        this.userService.retriveUsers().subscribe((orgall: BackendResponse) => {
          // console.log('>>>>>>> ' + JSON.stringify(orgall));
          this.userlist = orgall.data;
          if (this.userlist.length < 1) {
            let objtmp = {
              fullname: 'No records',
              userid: 'No records',
              active: 'No records',
              groupname: 'No records',
            };
            this.userlist = [];
            this.userlist.push(objtmp);
          }
          this.isFetching = false;
        });
      }
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedUser = data;
  }

  viewData(data: any) {
    console.log(data);
    this.viewDisplay = true;
    this.selectedUser = data;
  }

  approvalData(data: any) {
    // console.log(data);
    this.viewApprove = true;
    this.selectedUser = data;

  }
  approvalSubmit(status){
    // console.log(this.selectedUser);
    // console.log(status);
    let payload = {
      id: this.selectedUser.id,
      oldactive:this.selectedUser.active,
      isactive: status,
      idapproval: this.selectedUser.idapproval
    };
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.userService
      .updatebyAdminActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingUser();
          this.viewApprove = false;
        }
      });
  }

  deleteUser() {
    console.log(this.selectedUser);
    let user = this.selectedUser;
    const payload = { user };
    this.userService
      .deleteUserByAdmin(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingUser();
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
