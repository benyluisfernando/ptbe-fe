import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { GroupServiceService } from 'src/app/services/root/group-service.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-applicationgroup',
  templateUrl: './applicationgroup.component.html',
  styleUrls: ['./applicationgroup.component.scss'],
})
export class ApplicationgroupComponent implements OnInit {
  display = false;
  scrollheight:any ="400px"
  viewApprove = false;
  displayPrv = false;
  selectedgrp: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  grpheader: any = [
    { label: 'Group Name', sort: 'groupname' },
    { label: 'Status', sort: 'active' },
    { label: 'Created At', sort: 'created_at' },
  ];
  grpcolname: any = ['groupname','active', 'created_at'];
  grpcolhalign: any = ['','p-text-center','p-text-center'];
  grpcolwidth: any = [{ width: '670px' }, { width: '170px' },''];
  // grpcolwidth: any = ['', { width: '170px' }];
  grpcollinghref: any = { url: '#', label: 'Application' };
  grpactionbtn: any = [1, 1, 1, 1, 1, 1];
  grpaddbtn = { route: 'detail', label: 'Add Data' };
  grpanizations: any[] = [];
  groupsData: any[] = [];
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private groupService: GroupServiceService,
    private aclMenuService : AclmenucheckerService,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Roles' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then(dataacl =>{
          if(JSON.stringify(dataacl.acl) === "{}"){
            console.log("No ACL Founded")
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);
            this.grpactionbtn[0] = dataacl.acl.create;
              this.grpactionbtn[1] = dataacl.acl.read;
              this.grpactionbtn[2] = dataacl.acl.update;
              this.grpactionbtn[3] = dataacl.acl.delete;
              this.grpactionbtn[4] = dataacl.acl.view;
              this.grpactionbtn[5] = dataacl.acl.approval;
          }
        });
      });
    });
    this.refreshingApp();
  }
  refreshingApp() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh group ');
    //   if ((data.status = 200)) {
    //     this.grpanizationService
    //       .retrivegrpByTenant()
    //       .subscribe((grpall: BackendResponse) => {
    //         // console.log('>>>>>>> ' + JSON.stringify(grpall));
    //         this.grpanizations = grpall.data;
    //         if(this.grpanizations.length < 1){
    //           let objtmp = {"grpcode":"No records", "grpname":"No records","grpdescription":"No records","application":"No records","created_by":"No records"}
    //           this.grpanizations = [];
    //           this.grpanizations.push(objtmp);
    //         }

    //        this.isFetching=false;
    //       });
    //   }
    // });
    this.groupService.getAllGroup().subscribe((result: BackendResponse) => {
      // console.log('>>>>>>> ' + JSON.stringify(result));
      if (result.data.userGroup?.length > 0) {
        this.groupsData = [];
        this.groupsData = result.data.userGroup;
      } else {
        this.groupsData = [];
        let objtmp = { groupname: 'No records' };
        this.groupsData.push(objtmp);
      }
      this.isFetching = false;
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedgrp = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedgrp = data;
  }

  approvalData(data: any) {
    console.log(data);
    this.viewApprove = true;
    this.selectedgrp = data;
    
  }

  approvalSubmit(status){
    console.log(this.selectedgrp);
    console.log(status);
    
    let payload = {
      id: this.selectedgrp.id,
      oldactive:this.selectedgrp.active,
      isactive: status,
      idapproval: this.selectedgrp.idapproval
    };
    console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.groupService
      .editGroupActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingApp();
          this.viewApprove = false;
        }
      });
  }


  deleteGroup() {
    console.log(this.selectedgrp);
    let group = this.selectedgrp;
    const payload = { group };
    console.log('>> Di delete ' + JSON.stringify(payload));
    this.groupService
      .deleteGroup(payload)
      .subscribe((resp: BackendResponse) => {
        console.log('>> hasil Delete ' + resp);
        if (resp.status === 200) {
          this.showTopSuccess(resp.data);
        }
        this.display = false;
        this.refreshingApp();
      });
    // this.grpanizationService
    //   .deletegrp(payload)
    //   .subscribe((resp: BackendResponse) => {
    //     console.log(resp);
    //     if (resp.status === 200) {
    //       this.showTopSuccess(resp.data);
    //     }
    //     this.display = false;
    //     this.refreshingApp();
    //   });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
