import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { SysparamService } from 'src/app/services/komi/sysparam.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-systemparam',
  templateUrl: './systemparam.component.html',
  styleUrls: ['./systemparam.component.scss'],
})
export class SystemparamComponent implements OnInit {
  display = false;
  viewApprove = false;
  displayPrv = false;
  scrollheight: any = "400px";
  selectedsysParam: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  sysParamData: any[] = [];
  sysheader: any = [
    { label: 'Parameter Name', sort: 'paramname' },
    { label: 'Value', sort: 'paramvalua' },
    { label: 'Active', sort: 'active' },
    { label: 'Created At', sort: 'created_date' },
  ];
  syscolname: any = ['paramname', 'paramvalua', 'active', 'created_date'];
  syscolhalign: any = ['', '', 'p-text-center', 'p-text-center'];
  syscolwidth: any = ['', '', { width: '150px' }, ''];
  syscollinghref: any = { url: '#', label: 'Application' };
  sysactionbtn: any = [1, 1, 1, 1, 1, 1];
  sysaddbtn = { route: 'detail', label: 'Add Data' };
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private sysparamService: SysparamService,
    private aclMenuService : AclmenucheckerService,
    private router: Router
  ) {}
  // private sysparamService:sysparamService

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'System Parameters' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
        this.aclMenuService.getAclMenu(this.router.url).then(dataacl =>{
          if(JSON.stringify(dataacl.acl) === "{}"){
            console.log("No ACL Founded")
          } else {
            console.log("ACL Founded");
            console.log(dataacl.acl);
            this.sysactionbtn[0] = dataacl.acl.create;
              this.sysactionbtn[1] = dataacl.acl.read;
              this.sysactionbtn[2] = dataacl.acl.update;
              // this.sysactionbtn[3] = dataacl.acl.delete;
              this.sysactionbtn[4] = dataacl.acl.view;
              this.sysactionbtn[5] = dataacl.acl.approval;
          }
        });
      });
    });
    this.refreshingsysParam();
  }
  refreshingsysParam() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh sysParam ');
    this.sysparamService
      .getAllSysParByTenant()
      .subscribe((result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.sysParamData = [];
          let objtmp = {
            paramname: 'No records',
            paramvalua: 'No records',
            status: 'No records',
            created_date: 'No records',
          };
          this.sysParamData.push(objtmp);
        } else {
          this.sysParamData = result.data;
        }

        this.isFetching = false;
      });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedsysParam = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedsysParam = data;
  }

  deletesysParam() {
    console.log(this.selectedsysParam);
    let sysParam = this.selectedsysParam;
    const payload = { sysParam };
    console.log('>> Di delete ' + JSON.stringify(payload.sysParam));
    this.sysparamService
      .deleteSysParByTenant(payload.sysParam.id)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingsysParam();
      });
  }
  approvalData(data: any) {
    console.log(data);
    this.viewApprove = true;
    this.selectedsysParam = data;
    
  }
  approvalSubmit(status){
    console.log(this.selectedsysParam);
    console.log(status);
    let payload = {
      id: this.selectedsysParam.id,
      oldactive:this.selectedsysParam.active,
      isactive: status,
      idapproval: this.selectedsysParam.idapproval
    };
    // console.log('>>>>>>>> payload ' + JSON.stringify(payload));
    this.sysparamService
      .updateSysParByTenantActive(payload)
      .subscribe((result: BackendResponse) => {
        // console.log(">>>>>>>> return "+JSON.stringify(result));
        if (result.status === 200) {
          this.refreshingsysParam();
          this.viewApprove = false;
        }
      });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
