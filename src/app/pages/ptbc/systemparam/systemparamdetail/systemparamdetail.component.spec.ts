import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemparamdetailComponent } from './systemparamdetail.component';

describe('SystemparamdetailComponent', () => {
  let component: SystemparamdetailComponent;
  let fixture: ComponentFixture<SystemparamdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemparamdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemparamdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
