import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import {Location} from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { SysparamService } from 'src/app/services/komi/sysparam.service';

@Component({
  selector: 'app-systemparamdetail',
  templateUrl: './systemparamdetail.component.html',
  styleUrls: ['./systemparamdetail.component.scss']
})
export class SystemparamdetailComponent implements OnInit {
  extraInfo:any = {};
  isEdit:boolean= false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo:any = {};
  selectedApps: any = [];
  tokenID:string = "";
  groupForm!: FormGroup;
  old_code:any = "";
  submitted = false;
  constructor(private router: Router,private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location, private sysparamService:SysparamService) { this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
      let checkurl = this.extraInfo.indexOf("%23") !== -1?true:false;
      console.log(">>>>>>>>>>> "+this.extraInfo);
      console.log(checkurl);
      if(checkurl) this.isEdit=true;}

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'System Parameters',command: (event) => {
        this.location.back();
    }, url:"" }, { label: this.isEdit ?'Edit data':'Add data'}
    ];
    this.stateOptions = [
      { label: 'Active', value: 1 },
      { label: 'Inactive', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // syscolname:any = ["paramname", "paramvalua","status", "created_date"]
      this.groupForm = this.formBuilder.group({
        paramname: ['', Validators.required],
        paramvalua: ['', Validators.required],
        isactive: [0],
      });
      if(this.isEdit){ 
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.sysparamService.getSysParById(this.userId).subscribe(async (result: BackendResponse) => {
            // console.log("Data edit bic "+JSON.stringify(result.data));
            // if (this.isOrganization) {
              this.groupForm.patchValue({
                paramname: result.data.paramname,
                paramvalua: result.data.paramvalua,
                isactive: result.data.status,
              });
              this.old_code=result.data.id;
              // console.log(this.user);
          });

        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
      if (this.groupForm.valid) {
        // event?.preventDefault;
        // var groupacl = this.groupForm.get('orgobj')?.value;
        let payload ={};
        // bank_code, bic_code, bank_name
        if(!this.isEdit) {
          payload = {
            paramname: this.groupForm.get('paramname')?.value,
            paramvalua: this.groupForm.get('paramvalua')?.value,
            status: this.groupForm.get('isactive')?.value
          };
          console.log(">>>>>>>> payload "+JSON.stringify(payload));
          this.sysparamService.insertSysParByTenant(payload).subscribe((result: BackendResponse) => {
              // if (result.status === 200) {
              //   this.location.back();
              // }
              // console.log(">>>>>>>> payload "+JSON.stringify(result));
              this.location.back();
          });
        } else {
          payload = {
            paramname: this.groupForm.get('paramname')?.value,
            paramvalua: this.groupForm.get('paramvalua')?.value,
            status: this.groupForm.get('isactive')?.value,
            id:this.old_code
          };
          this.sysparamService.updateSysParByTenant(payload).subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            // console.log(">>>>>>>> payload "+JSON.stringify(result));
            this.location.back();
        });
        }
      }
    

    console.log(this.groupForm.valid);
  }
}
