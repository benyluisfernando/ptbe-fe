import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingCustomerTypeComponent } from './mapping-customer-type.component';

describe('MappingCustomerTypeComponent', () => {
  let component: MappingCustomerTypeComponent;
  let fixture: ComponentFixture<MappingCustomerTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingCustomerTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingCustomerTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
