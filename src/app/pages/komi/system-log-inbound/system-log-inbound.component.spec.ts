import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemLogInboundComponent } from './system-log-inbound.component';

describe('SystemLogInboundComponent', () => {
  let component: SystemLogInboundComponent;
  let fixture: ComponentFixture<SystemLogInboundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SystemLogInboundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemLogInboundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
