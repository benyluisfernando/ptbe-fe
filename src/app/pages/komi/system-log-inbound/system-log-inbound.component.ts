import { Component, OnInit } from '@angular/core';

import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';
import { EventlogService } from 'src/app/services/komi/eventlog.service';
import { error } from 'console';

@Component({
  selector: 'app-system-log-inbound',
  templateUrl: './system-log-inbound.component.html',
  styleUrls: ['./system-log-inbound.component.scss'],
})
export class SystemLogInboundComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedProxy: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';
  limitData: any[] = [];

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private eventService: EventlogService
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Inbound Log' }];
    this.refreshingProxy();
  }
  refreshingProxy() {
    this.isFetching = true;
    this.eventService.getInboundLog().subscribe(
      async (result: BackendResponse) => {
        this.limitData = result.data;
        this.isFetching = false;
      },
      (error: any) => {
        this.showTopErr(error.error.data.message);
        this.isFetching = false;
      }
    );
  }

  showTopErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }
}
