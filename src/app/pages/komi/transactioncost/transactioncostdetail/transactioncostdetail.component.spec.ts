import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactioncostdetailComponent } from './transactioncostdetail.component';

describe('TransactioncostdetailComponent', () => {
  let component: TransactioncostdetailComponent;
  let fixture: ComponentFixture<TransactioncostdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactioncostdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactioncostdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
