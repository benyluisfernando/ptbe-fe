import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-bicadministration',
  templateUrl: './bicadministration.component.html',
  styleUrls: ['./bicadministration.component.scss']
})
export class BicadministrationComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedbic: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  userInfo:any = {};
  tokenID:string = "";
  bicData: any[] = [];
  // bank_code, bic_code, bank_name, last_update_date, created_date, change_who, idtenant
  bicheader:any = [{'label':'Bank Code', 'sort':'bank_code'},{'label':'BIC Code', 'sort': 'bic_code'}, {'label':'Bank Name', 'sort': 'bank_name'}, {'label':'Created At', 'sort': 'created_date'}];
  biccolname:any = ["bank_code", "bic_code","bank_name", "created_date"]
  biccolhalign:any = ["p-text-center","p-text-center","","p-text-center"]
  biccolwidth:any = [{'width':'170px'},{'width':'170px'},"",{'width':'170px'}];
  biccollinghref:any = {'url':'#','label':'Application'}
  bicactionbtn:any = [1,1,1,1,0,0]
  bicaddbtn ={'route':'detail', 'label':'Add Data'}
  constructor(private authservice:AuthService,public dialogService: DialogService,
    public messageService: MessageService, private bicadminService:BicadminService,private aclMenuService : AclmenucheckerService,
    private router: Router) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'BIC Administrator' }
    ];
    this.authservice.whoAmi().subscribe((value) =>{
      // console.log(">>> User Info : "+JSON.stringify(value));
        this.userInfo = value.data;
        this.tokenID = value.tokenId; 
        this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
          console.log("MENU ALL ACL Set");
          this.aclMenuService.getAclMenu(this.router.url).then(dataacl =>{
            if(JSON.stringify(dataacl.acl) === "{}"){
  
            } else {
              console.log(dataacl.acl);
              this.bicactionbtn[0] = dataacl.acl.create;
              this.bicactionbtn[1] = dataacl.acl.read;
              this.bicactionbtn[2] = dataacl.acl.update;
              this.bicactionbtn[3] = dataacl.acl.delete;
              this.bicactionbtn[4] = dataacl.acl.view;
              this.bicactionbtn[5] = dataacl.acl.approval;
            }
          });
        });
    });
    this.refreshingBic();
  }
  refreshingBic() {
    this.isFetching= true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> Refresh BIC ");
      // console.log(">>> User Info : "+JSON.stringify(this.userInfo));
     



    this.bicadminService.getAllBicByTenant().subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if(result.status === 202) {
          this.bicData = [];
          let objtmp = {"bank_code":"No records","bic_code":"No records", "bank_name":"No records", "created_date" : "No records"};
          this.bicData.push(objtmp);
      } else {
        this.bicData = result.data.bicAdmins;
      }
      this.isFetching=false;
    });
  }
  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.display = true;
    this.selectedbic = data;
  }

  previewConfirmation(data:any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.displayPrv = true;
    this.selectedbic = data;
  }


  deleteBic() {
    console.log(this.selectedbic);
    let bic = this.selectedbic;
    const payload = {bic};
    console.log(">> Di delete "+JSON.stringify(payload.bic))
    this.bicadminService.deleteBicByTenant(payload.bic.id).subscribe((resp: BackendResponse)=>{
        this.display = false;
        this.showTopSuccess(resp.data);
      this.refreshingBic();
    });
    // this.bicadminService.deleteGroup(payload).subscribe((resp: BackendResponse)=>{
    //   console.log(">> hasil Delete "+ resp);
    //       if (resp.status === 200) {
    //       this.showTopSuccess(resp.data);
    //     }
    //   this.display = false;
    //   this.refreshingBic();
    // });
    
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
