import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KomihomeComponent } from './komihome.component';

describe('KomihomeComponent', () => {
  let component: KomihomeComponent;
  let fixture: ComponentFixture<KomihomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KomihomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KomihomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
