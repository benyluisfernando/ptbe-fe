import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { PrefundDashboardService } from 'src/app/services/komi/prefund-dashboard.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import {any} from 'codelyzer/util/function';
import { AclmenucheckerService } from 'src/app/services/utils/aclmenuchecker.service';

@Component({
  selector: 'app-komihome',
  templateUrl: './komihome.component.html',
  styleUrls: ['./komihome.component.scss'],

})
export class KomihomeComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  userInfo: any = {};
  app: any = {};
  tokenID: string = '';
  home!: MenuItem;
  param: string;
  basicOptions: {
    plugins: { legend: { labels: { color: string } } };
    scales: {
      x: { ticks: { color: string }; grid: { color: string } };
      y: { ticks: { color: string }; grid: { color: string } };
    };
  };
  basicData: {
    labels: string[];
    datasets: {
      label: string;
      data: number[];
      fill: boolean;
      borderColor: string;
      tension: number;
    }[];
  };

  gaugeType = 'semi';
  gaugeValue = 6000;
  gaugeMax = 0;
  gaugeMin = 0;
  // gaugeAppendText = `/${this.gaugeMax}`;


  thresholdConfig = {

  };

  // 1000: {color: 'red'},
  // 1300: {color: 'orange'},
  // 5000: {color: 'green'}


  usrheader: any = [
    { label: 'No', sort: 'id' },
    { label: 'Surrounding System', sort: 'surrounding_system' },
    { label: 'Status', sort: 'status' },
  ];
  usrcolname: any = ['id', 'surrounding_system', 'status'];
  usrcolhalign: any = ['p-text-center', 'p-text-center', 'p-text-center'];
  userlist: any = [
    {
      id: 1,
      surrounding_system: 'Core Banking',
      status: 'Active',
    },
    {
      id: 2,
      surrounding_system: 'CI-Connector',
      status: 'Active',
    },
  ];

  payload: any = {
    transactionId: '000001',
    dateTime: '2021-10-21T11:00:00.000',
    merchantType: '6018',
    terminalId: 'MBS       ',
    noRef: '20200420471644500911',
    accountNumber: '1112223'
  };

  usrcolwidth: any = ['', '', '', '', ''];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [0, 0, 0, 0, 0];
  usraddbtn = { route: 'detail', label: 'Add Data' };
  categoryChart: { name: string; code: string }[];
  selectedCategoryChart: any = { name: 'Daily Chart', code: 'daily' };


  constructor(
    private route: ActivatedRoute,
    private authservice: AuthService,
    private prefundDashboardService: PrefundDashboardService,
    private aclMenuService : AclmenucheckerService
  ) {}

  ngOnInit(): void {
    this.param = this.route.snapshot.params.config;
    // console.log('>>>>'+this.param);
    console.log('>>>> INIIIIIIIIIIIIIIIT HOME');
    this.home = { icon: 'pi pi-home', routerLink: '/' };
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.app = this.userInfo.apps[0];
      this.aclMenuService.setAllMenus(this.userInfo.sidemenus).then(data => {
        console.log("MENU ALL ACL Set");
      });



    });
    this.getProp();
    this.getBalanceInquiry();
    this.categoryChart = [
      { name: 'Daily Chart', code: 'daily' },
      { name: 'Weekly Chart', code: 'weekly' },
      { name: 'Monthly Chart', code: 'monthly' },
    ];
  }

  getProp() {
    this.prefundDashboardService
      .getDashboardProp()
      .subscribe((result: BackendResponse) => {
        this.gaugeMax = result.data.dashboardProp.max;
        this.gaugeMin = result.data.dashboardProp.min;
        // this.gaugeValue = result.data.dashboardProp.current;
        // this.gaugeAppendText = `/${this.gaugeMax}`;
        let dataMin10 = +(this.gaugeMin * 10 / 100) + +this.gaugeMin; //100
        this.thresholdConfig[dataMin10] = {color: 'red'};
        let dataMin20 = +(this.gaugeMin * 20 / 100) + +this.gaugeMin; //20%
        this.thresholdConfig[dataMin20] = {color: 'orange'};


        console.log('++++++++++++', dataMin10, dataMin20);

        console.log(result.data);
      });
  }

  getBalanceInquiry() {
    this.prefundDashboardService
      .balanceInquiry(this.payload)
      .subscribe((result: any) => {
        this.gaugeValue = result.balance;

        console.log(result.balance);
      });
  }


}
