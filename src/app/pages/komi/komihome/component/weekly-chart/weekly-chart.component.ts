import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weekly-chart',
  templateUrl: './weekly-chart.component.html',
  styleUrls: ['./weekly-chart.component.scss'],
})
export class WeeklyChartComponent implements OnInit {
  userInfo: any = {};
  app: any = {};
  param: string;
  basicOptions: {
    plugins: { legend: { labels: { color: string } } };
    scales: {
      x: { ticks: { color: string }; grid: { color: string } };
      y: { ticks: { color: string }; grid: { color: string } };
    };
  };
  basicData: {
    labels: string[];
    datasets: {
      label: string;
      data: number[];
      fill: boolean;
      borderColor: string;
      tension: number;
    }[];
  };

  gaugeType = 'semi';
  gaugeValue = 28;
  gaugeMax = 100;
  gaugeMin = 0;
  gaugeAppendText = `/${this.gaugeMax}`;

  usrheader: any = [
    { label: 'No', sort: 'id' },
    { label: 'Surrounding System', sort: 'surrounding_system' },
    { label: 'Status', sort: 'status' },
  ];
  usrcolname: any = ['id', 'surrounding_system', 'status'];
  usrcolhalign: any = ['p-text-center', 'p-text-center', 'p-text-center'];
  userlist: any = [
    {
      id: 1,
      surrounding_system: 'Core',
      status: 'Active',
    },
    {
      id: 2,
      surrounding_system: 'System 2',
      status: 'Active',
    },
    {
      id: 3,
      surrounding_system: 'System 3',
      status: 'In-Active',
    },
  ];

  usrcolwidth: any = ['', '', '', '', ''];
  // orgcollinghref:any = {'url':'#','label':'Application'}
  usractionbtn: any = [0, 0, 0, 0, 0];
  usraddbtn = { route: 'detail', label: 'Add Data' };
  categoryChart: { name: string; code: string }[];
  selectedCategoryChart: any = { name: 'Daily Chart', code: 'daily' };

  constructor() {}

  ngOnInit(): void {
    this.initChart();
  }

  initChart() {
    this.basicData = {
      labels: [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'ThursDay',
        'Friday',
        'Saturday',
      ],
      datasets: [
        {
          label: 'Success Transaction',
          data: [65, 59, 80, 81, 56, 55, 100],
          fill: false,
          borderColor: '#537f2d',
          tension: 0.4,
        },
        {
          label: 'Timeout Transaction',
          data: [60, 29, 10, 1, 40, 75, 40],
          fill: false,
          borderColor: '#e8a704',
          tension: 0.4,
        },
        {
          label: 'Failed Transaction',
          data: [28, 48, 40, 19, 86, 27, 90],
          fill: false,
          borderColor: '#D32F2F',
          tension: 0.4,
        },
      ],
    };
  }
}
