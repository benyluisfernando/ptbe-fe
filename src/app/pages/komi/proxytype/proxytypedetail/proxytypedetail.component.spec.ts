import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxytypedetailComponent } from './proxytypedetail.component';

describe('ProxytypedetailComponent', () => {
  let component: ProxytypedetailComponent;
  let fixture: ComponentFixture<ProxytypedetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProxytypedetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProxytypedetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
