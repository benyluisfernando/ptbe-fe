import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { Location } from '@angular/common';
import { ProxytypeService } from 'src/app/services/komi/proxytype.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';

@Component({
  selector: 'app-proxytypedetail',
  templateUrl: './proxytypedetail.component.html',
  styleUrls: ['./proxytypedetail.component.scss'],
})
export class ProxytypedetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  old_code: any = '';
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private proxytypeService: ProxytypeService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Proxy Type',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.stateOptions = [
      { label: 'Active', value: 1 },
      { label: 'Inactive', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      this.groupForm = this.formBuilder.group({
        proxycode: ['', Validators.required],
        proxyname: ['', Validators.required],
        isactive: [0],
      });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.proxytypeService
            .getPrxTById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit bic ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              // "proxycode", "proxyname", "status", "created_date"
              this.groupForm.patchValue({
                proxycode: result.data.proxycode,
                proxyname: result.data.proxyname,
                isactive: result.data.status,
              });
              this.old_code = result.data.id;
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      // bank_code, bic_code, bank_name
      if (!this.isEdit) {
        payload = {
          proxycode: this.groupForm.get('proxycode')?.value,
          proxyname: this.groupForm.get('proxyname')?.value,
          status: this.groupForm.get('isactive')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.proxytypeService
          .insertPrxTByTenant(payload)
          .subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
            this.location.back();
          });
      } else {
        payload = {
          proxycode: this.groupForm.get('proxycode')?.value,
          proxyname: this.groupForm.get('proxyname')?.value,
          status: this.groupForm.get('isactive')?.value,
          id: this.old_code,
        };
        this.proxytypeService
          .updatePrxTByTenant(payload)
          .subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
            this.location.back();
          });
      }
    }

    console.log(this.groupForm.valid);
  }
}
