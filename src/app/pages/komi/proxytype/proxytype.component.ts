import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { ProxytypeService } from 'src/app/services/komi/proxytype.service';

@Component({
  selector: 'app-proxytype',
  templateUrl: './proxytype.component.html',
  styleUrls: ['./proxytype.component.scss']
})
export class ProxytypeComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedchn: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  userInfo:any = {};
  tokenID:string = "";
  prxData: any[] = [];
  // channeltype, status created_date

  // proxycode, proxyname, status, change_who, idtenant

  prxheader:any = [{'label':'Proxy Code', 'sort':'proxycode'}, {'label':'Proxy Name', 'sort': 'proxyname'},{'label':'Active', 'sort': 'status'}, {'label':'Created At', 'sort': 'created_date'}];
  prxcolname:any = ["proxycode", "proxyname", "status", "created_date"]
  prxcolhalign:any = ["p-text-center","","p-text-center","p-text-center",]
  // chncolwidth:any = [{'width':'170px'},{'width':'170px'},"",{'width':'170px'}];
  prxcolwidth:any = [{'width':'170px'},"",{'width':'170px'},{'width':'170px'}];
  prxcollinghref:any = {'url':'#','label':'Application'}
  prxactionbtn:any = [1,1,1,1,0]
  prxaddbtn ={'route':'detail', 'label':'Add Data'}
  constructor(private authservice:AuthService,public dialogService: DialogService,
    public messageService: MessageService,private proxytypeService:ProxytypeService ) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Proxy Type' }
    ];
    this.authservice.whoAmi().subscribe((value) =>{
      // console.log(">>> User Info : "+JSON.stringify(value));
        this.userInfo = value.data;
        this.tokenID = value.tokenId; 
    });
    this.refreshingPrx();
  }
  refreshingPrx() {
    this.isFetching= true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      console.log(">>>>>>> Refresh Prx ");
    this.proxytypeService.getAllPrxTByTenant().subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
    // var query = "SELECT id, channelcode, channeltype, status, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, last_update_date, idtenant FROM public.m_channeltype WHERE idtenant=$1 order by created_date desc";

      if(result.status === 202) {
          this.prxData = [];
          let objtmp = {"proxycode":"No records","proxyname":"No records", "status":"No records", "created_date" : "No records"};
          this.prxData.push(objtmp);
      } else {
        this.prxData = result.data;
      }
      this.isFetching=false;
    });
  }
  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.display = true;
    this.selectedchn = data;
  }

  previewConfirmation(data:any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.displayPrv = true;
    this.selectedchn = data;
  }


  deletePrx() {
    console.log(this.selectedchn);
    let prx = this.selectedchn;
    const payload = {prx};
    console.log(">> Di delete "+JSON.stringify(payload.prx))
    this.proxytypeService.deletePrxTByTenant(payload.prx.id).subscribe((resp: BackendResponse)=>{
        this.display = false;
        this.showTopSuccess(resp.data);
      this.refreshingPrx();
    });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
