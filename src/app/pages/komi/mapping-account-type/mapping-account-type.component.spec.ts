import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingAccountTypeComponent } from './mapping-account-type.component';

describe('MappingAccountTypeComponent', () => {
  let component: MappingAccountTypeComponent;
  let fixture: ComponentFixture<MappingAccountTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingAccountTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingAccountTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
