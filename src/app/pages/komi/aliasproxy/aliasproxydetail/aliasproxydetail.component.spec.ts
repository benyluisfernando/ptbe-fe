import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AliasproxydetailComponent } from './aliasproxydetail.component';

describe('AliasproxydetailComponent', () => {
  let component: AliasproxydetailComponent;
  let fixture: ComponentFixture<AliasproxydetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AliasproxydetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AliasproxydetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
