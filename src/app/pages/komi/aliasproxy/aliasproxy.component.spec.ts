import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AliasproxyComponent } from './aliasproxy.component';

describe('AliasproxyComponent', () => {
  let component: AliasproxyComponent;
  let fixture: ComponentFixture<AliasproxyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AliasproxyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AliasproxyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
