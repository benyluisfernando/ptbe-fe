import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrefundmanagerComponent } from './prefundmanager.component';

describe('PrefundmanagerComponent', () => {
  let component: PrefundmanagerComponent;
  let fixture: ComponentFixture<PrefundmanagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrefundmanagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrefundmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
