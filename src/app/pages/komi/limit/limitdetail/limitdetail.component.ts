import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, SelectItem } from 'primeng/api';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';

@Component({
  selector: 'app-limitdetail',
  templateUrl: './limitdetail.component.html',
  styleUrls: ['./limitdetail.component.scss'],
})
export class LimitdetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  proxytypes: any = [];
  proxystates: any = [];
  registrarbics: any = [];
  idtypes: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  items: SelectItem[];
  item: string;
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private proxyadminService: ProxyadminService,
    private bussinesparamService: BussinesparamService,
    private bicadminService: BicadminService,
    private limitService: LimitService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Master Limit',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // registration_id, proxy_type, proxy_value, registrar_bic, account_number, account_type, account_name, identification_number_type, identification_number_value, proxy_status
      this.groupForm = this.formBuilder.group({
        limit_name: ['', Validators.required],
        tran_type: ['', Validators.required],
        min_tran: ['', Validators.required],
        max_limit: ['', Validators.required],
        status: ['', Validators.required],
      });

      this.proxytypes = [];
      this.idtypes = [];
      this.registrarbics = [];

      this.bussinesparamService
        .getAllByCategory('KOMI_LMT_STS')
        .subscribe((result: BackendResponse) => {
          console.log('>>>>>>> ' + JSON.stringify(result));
          if (result.status === 200) {
            this.proxytypes = result.data;
          } else {
            // this.proxyData = result.data.bicAdmins;
          }
        });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.limitService
            .getById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              console.log('Data edit bic ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.groupForm.patchValue({
                limit_name: result.data.limit.limit_name,
                tran_type: result.data.limit.tran_type,
                min_tran: result.data.limit.min_tran,
                max_limit: result.data.limit.max_limit,
                status: result.data.limit.status,
              });
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      console.log(this.groupForm);

      if (!this.isEdit) {
        payload = {
          limit_name: this.groupForm.get('limit_name')?.value,
          tran_type: this.groupForm.get('tran_type')?.value,
          min_tran: this.groupForm.get('min_tran')?.value,
          max_limit: this.groupForm.get('max_limit')?.value,
          status: this.groupForm.get('status')?.value,
        };
        this.limitService
          .insert(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              this.location.back();
            }
          });
      } else {
        payload = {
          id: this.userId,
          limit_name: this.groupForm.get('limit_name')?.value,
          tran_type: this.groupForm.get('tran_type')?.value,
          min_tran: this.groupForm.get('min_tran')?.value,
          max_limit: this.groupForm.get('max_limit')?.value,
          status: this.groupForm.get('status')?.value,
        };
        this.limitService
          .update(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              this.location.back();
            }
          });
      }

      // bank_code, bic_code, bank_name
      // if (!this.isEdit) {
      //   payload = {
      //     bank_code: this.groupForm.get('bank_code')?.value,
      //     bic_code: this.groupForm.get('bic_code')?.value,
      //     bank_name: this.groupForm.get('bank_name')?.value,
      //   };
      //   console.log('>>>>>>>> payload ' + JSON.stringify(payload));
      //   // this.bicadminService.insertBicByTenant(payload).subscribe((result: BackendResponse) => {
      //   //     // if (result.status === 200) {
      //   //     //   this.location.back();
      //   //     // }
      //   //     console.log(">>>>>>>> payload "+JSON.stringify(result));
      //   //     this.location.back();
      //   // });
      // } else {
      //   // payload = {
      //   //   fullname: this.groupForm.get('fullname')?.value,
      //   //   userid: this.userId,
      //   //   group: this.groupForm.get('orgMlt')?.value,
      //   //   isactive: this.groupForm.get('isactive')?.value,
      //   // }
      //   // console.log(">>>>>>>> payload "+JSON.stringify(payload));
      //   // this.umService
      //   // .updatebyAdmin(payload)
      //   // .subscribe((result: BackendResponse) => {
      //   //   // console.log(">>>>>>>> return "+JSON.stringify(result));
      //   //   if (result.status === 200) {
      //   //     this.location.back();
      //   //   }
      //   // });
      //   // this.location.back();
      // }
    }

    console.log(this.groupForm.valid);
  }

  format(event) {
    console.log(event.target.id);
    let currVal = event.target.value;

    var number_string = currVal.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      let separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    console.log('Rp. ' + rupiah);
    this.groupForm.patchValue({ [event.target.id]: 'Rp. ' + rupiah });
  }
}
