import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LimitdetailComponent } from './limitdetail.component';

describe('LimitdetailComponent', () => {
  let component: LimitdetailComponent;
  let fixture: ComponentFixture<LimitdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LimitdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LimitdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
