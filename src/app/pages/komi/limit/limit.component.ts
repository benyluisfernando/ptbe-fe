import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';

@Component({
  selector: 'app-limit',
  templateUrl: './limit.component.html',
  styleUrls: ['./limit.component.scss'],
})
export class LimitComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedProxy: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';
  limitData: any[] = [];
  prxheader: any = [
    { label: 'Limit Name', sort: 'limit_name' },
    { label: 'Transaction Type', sort: 'tran_type' },
    { label: 'Minimum Transaction', sort: 'min_tran' },
    { label: 'Maximum Limit', sort: 'max_limit' },
    { label: 'Status', sort: 'status' },
  ];
  prxcolname: any = [
    'limit_name',
    'tran_type',
    'min_tran',
    'max_limit',
    'status',
  ];
  prxcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  prxcolwidth: any = ['', '', '', '', '', '', '', '', ''];
  prxcollinghref: any = { url: '#', label: 'Application' };
  limitactionbtn: any = [1, 1, 1, 1, 0];
  prxaddbtn = { route: 'detail', label: 'Add Data' };
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private limitService: LimitService
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Master Limit' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();
  }
  refreshingProxy() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh Limit ');
    this.limitService.getAll().subscribe(async (result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if (result.status === 202) {
        this.limitData = [];

        let objtmp = {
          limit_name: 'No records',
          tran_type: 'No records',
          min_tran: 'No records',
          max_limit: 'No records',
          status: 'No records',
        };
        this.limitactionbtn = [1, 0, 0, 0, 0];
        this.limitData.push(objtmp);
      } else {
        if (result.data.limit.length > 0) {
          await result.data.limit.map((dt) => {
            var reverse, resultRP;
            dt.status =
              dt.status == 1
                ? 'Active'
                : dt.status == 2
                ? 'InActive'
                : 'InActive';
            (reverse = dt.min_tran.toString().split('').reverse().join('')),
              (resultRP = reverse.match(/\d{1,3}/g));
            resultRP = resultRP.join('.').split('').reverse().join('');
            dt.min_tran = 'RP ' + resultRP;
            (reverse = dt.max_limit.toString().split('').reverse().join('')),
              (resultRP = reverse.match(/\d{1,3}/g));
            resultRP = resultRP.join('.').split('').reverse().join('');
            dt.max_limit = 'RP ' + resultRP;
          });

          this.limitData = result.data.limit;
        }
      }
      // if(result.data.userGroup.length > 0) {
      //   this.proxyData = [];
      //   this.proxyData = result.data.userGroup;
      // } else {
      //   this.proxyData = [];
      //   let objtmp = {"groupname":"No records"};
      //   this.proxyData.push(objtmp);
      // }
      this.isFetching = false;
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedProxy = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedProxy = data;
  }

  deleteLimit() {
    console.log(this.selectedProxy);
    this.limitService
      .delete(this.selectedProxy.id)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingProxy();
      });
    // this.proxyadminService.deleteGroup(payload).subscribe((resp: BackendResponse)=>{
    //   console.log(">> hasil Delete "+ resp);
    //       if (resp.status === 200) {
    //       this.showTopSuccess(resp.data);
    //     }
    //   this.display = false;
    //   this.refreshingBic();
    // });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
