import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { AdminnotificationService } from 'src/app/services/komi/adminnotification.service';

@Component({
  selector: 'app-adminnotification',
  templateUrl: './adminnotification.component.html',
  styleUrls: ['./adminnotification.component.scss'],
})
export class AdminnotificationComponent implements OnInit {
  
  display = false;
  displayPrv = false;
  selectedbic: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  admnotiflist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';

  bicheader: any = [
    { label: 'Full Name', sort: 'fullname' },
    { label: 'Email', sort: 'email_user' },
    { label: 'Devisi', sort: 'devisi' },
    { label: 'Notifies For', sort: 'cd_busparam' },
    { label: 'Created Date', sort: 'created_at' },
  ];
  biccolname: any = [
    'fullname',
    'email_user',
    'devisi',
    'cd_busparam',
    'created_at',
  ];
  biccolhalign: any = ['', '', 'p-text-center', '', '', 'p-text-center'];
  biccolwidth: any = ['', '', '', { width: '150px' }, '', { width: '150px' }];
  biccollinghref: any = { url: '#', label: 'Application' };
  bicactionbtn: any = [1, 1, 1, 1, 0];
  bicaddbtn = { route: 'detail', label: 'Add Data' };

  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private admNotService: AdminnotificationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Admin Notification' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      // console.log(">>> Router Info : "+this.router.url);
      this.userInfo = value.data;
      // let allmenus = this.userInfo.sidemenus;
      this.tokenID = value.tokenId;
    });
    this.refreshingUser();
  }
  refreshingUser() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    // console.log(">>>>>>> "+JSON.stringify(data));
    // if ((data.status = 200)) {
    this.admNotService
      .getAllAdminNotification()
      .subscribe((admnotifs: BackendResponse) => {
        // console.log('>>>>>>> ' + JSON.stringify(admnotifs));
        this.admnotiflist = admnotifs.data;
        if (this.admnotiflist.length < 1) {
          let objtmp = {
            iduser: 'No records',
            fullname: 'No records',
            userid: 'No records',
            active: 'No records',
            groupname: 'No records',
            cd_busparam: 'No records',
            created_at: 'No records',
          };
          this.admnotiflist = [];
          this.admnotiflist.push(objtmp);
        }
        this.isFetching = false;
      });
    // }
    // });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedbic = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    // this.displayPrv = true;
    // this.selectedbic = data;
  }

  deleteBic() {
    console.log(this.selectedbic);
    let payload = {
      id: this.selectedbic.id,
      iduser: this.selectedbic.id
    };
    this.admNotService
      .deleteData(payload)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingUser();
      });
  }
 
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
  

}
