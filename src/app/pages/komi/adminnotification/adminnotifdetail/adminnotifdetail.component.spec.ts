import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminnotifdetailComponent } from './adminnotifdetail.component';

describe('AdminnotifdetailComponent', () => {
  let component: AdminnotifdetailComponent;
  let fixture: ComponentFixture<AdminnotifdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminnotifdetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminnotifdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
