import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { DatePipe } from '@angular/common';
// import { Table } from 'primeng/table';
import { AuthService } from 'src/app/services/auth.service';
import { TrxmonitorService } from 'src/app/services/komi/trxmonitor.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { ChanneltypeService } from 'src/app/services/komi/channeltype.service';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { element } from 'protractor';
import { SysparamService } from 'src/app/services/komi/sysparam.service';

@Component({
  selector: 'app-transactionmonitor',
  templateUrl: './transactionmonitor.component.html',
  styleUrls: ['./transactionmonitor.component.scss'],
})
export class TransactionmonitorComponent implements OnInit {
  // customers: Customer[];

  rangeDates: Date[];
  isdataexist = false;
  isactives: any = [];
  isactivesSelected: any = {};
  channels: any = [];
  channelSelected: any = {};
  bifastTrxNo: any;
  transactiontype: any = [];
  transactiontypeSelected: any = {};
  fileName = 'ExcelSheet.xlsx';
  startDate: any = new Date();
  endDate: any;
  display = false;
  displayPrv = false;
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';
  logData: any[] = [];
  dateTime: any;
  dateNow: any;
  birthday: any;
  isLoggedIn: boolean = false;
  sysParamData: any[] = [];
  range: number;
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,
    private trxmonitorService: TrxmonitorService,
    private channeltypeService: ChanneltypeService,
    private sysparamService: SysparamService
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Transaction Monitoring' }];

    this.channeltypeService
      .getAllChnByTenant()
      .subscribe((result: BackendResponse) => {
        if (result.status == 200) {
          console.log(JSON.stringify(result.data));
          this.channels = result.data;
          // this.channels=[];
        } else {
          this.channels = [];
        }
      });
    // reff number
    console.log('>>>>>>> Refresh sysParam ');
    this.range = 0;
    this.sysparamService
      .getAllSysParByTenant()
      .subscribe((result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.sysParamData = [];
          let objtmp = {
            paramname: 'No records',
            paramvalua: 'No records',
            status: 'No records',
            created_date: 'No records',
          };
          this.sysParamData.push(objtmp);
        } else {
          // KOMI_CORE_TRX_DETAIL_RANGE
          this.sysParamData = result.data;
          for (let i = 0; i < this.sysParamData.length; i++) {
            if (
              this.sysParamData[i].paramname == 'KOMI_PORTAL_TRX_DETAIL_RANGE'
            ) {
              this.range = this.sysParamData[i].paramvalua;
            }
          }
        }
      });

    this.isactives = [
      { label: 'Success', value: 'S' },
      { label: 'Failed', value: 'F' },
      { label: 'Exception', value: 'E' },
      { label: 'Pending', value: 'P' },
    ];
    this.transactiontype = [
      { label: 'Incoming Transaction', value: 'I' },
      { label: 'Outgoing Transaction', value: 'O' },
    ];
    // this.logData = [{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"},{created_date : "2021-01-01",status:"success",trxnumber:"1231234234235235", refnumbifast:"99994234235235", channel:"Mobile banking", destbank:"Bank BCA", srcbank:"Bank Mantap", destaccnum:"1231234234235235", srcaccnum:"23452345234523452345", destaccname:"Jhony Kuple", srcaccname:"Deden dandan", change_who:"SYS", amount:1000000, transacttype:"Incoming"}];
    this.logData = [];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.initialData();
  }

  refreshData() {
    this.isdataexist = false;
    this.logData = [];
    this.isFetching = true;
    let payload: any = {};
    // if (this.rangeDates != undefined) {
    //   start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
    //   end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
    // }
    // if (start_date) payload.start_date = start_date;
    // if (end_date) payload.end_date = end_date;

    if (this.startDate) {
      payload.startDate = this.datepipe.transform(this.startDate, 'yyyy-MM-dd');
    }
    // if (this.endDate) {
    // payload.endDate = this.datepipe.transform(this.endDate, 'yyyy-MM-dd');
    // }
    this.dateTime = new Date(
      this.startDate.getFullYear(),
      this.startDate.getMonth(),
      this.startDate.getDate() + Number(this.range)
    );

    if (this.endDate > this.dateTime) {
      this.isLoggedIn = true;
      this.endDate = this.dateTime;
      payload.endDate = this.datepipe.transform(this.dateTime, 'yyyy-MM-dd');
    } else {
      this.isLoggedIn = false;
      payload.endDate = this.datepipe.transform(this.endDate, 'yyyy-MM-dd');
    }

    this.isLoggedIn = false;

    if (this.isactivesSelected) payload.status = this.isactivesSelected.value;
    if (this.channelSelected)
      payload.channel = this.channelSelected.channelcode;
    if (this.bifastTrxNo) payload.bifasttrxno = this.bifastTrxNo;
    if (this.transactiontypeSelected)
      payload.trxtype = this.transactiontypeSelected.value;

    // console.log("Range {"+start_date+"-"+end_date+"}");
    // console.log("Status : "+JSON.stringify(this.isactivesSelected));
    // console.log("Channel : "+JSON.stringify(this.channelSelected));
    // console.log("TransType : "+JSON.stringify(this.transactiontypeSelected));
    console.log('JSON>>>', JSON.stringify(payload));

    this.trxmonitorService
      .postParam(payload)
      .subscribe((result: BackendResponse) => {
        console.log(JSON.stringify(result));
        if (result.status == 200) {
          this.isdataexist = true;
          this.logData = result.data;
        } else {
          this.isdataexist = false;
          this.logData = [];
          // let objtmp = {
          //   created_date: 'No record',
          //   status: 'No record',
          //   trxnumber: 'No record',
          //   refnumbifast: 'cek',
          //   channel: 'No record',
          //   destbank: 'No record',
          //   srcbank: 'No record',
          //   destaccnum: 'No record',
          //   srcaccnum: 'No record',
          //   destaccname: 'No record',
          //   srcaccname: 'No record',
          //   change_who: 'No record',
          //   amount: 0,
          //   transacttype: 'No record',
          // };
          let objtmp = {
            id: 'No record',
            komi_unique_id: 'No record', // trx id
            bifast_trx_no: 'No record', // ref number
            komi_trx_no: 'No record',
            channel_type: 'No record', // chanel
            branch_code: 'No record', //
            recipient_bank: 'No record', // reciver bank code
            sender_bank: 'No record', // sender bank code
            recipient_account_no: 'No record', // receive acc num
            recipient_proxy_type: 'No record',
            recipient_proxy_alias: 'No record',
            recipient_account_name: 'No record', // seceive acc name
            sender_account_no: 'No record', // sender acc num
            sender_account_name: 'No record', // sender acc name
            charge_type: 'No record',
            trx_type: 'No record', //
            trx_amount: 'No record',
            trx_fee: 'No record',
            trx_initiation_date: 'No record', // initiation date
            trx_status_code: 'No record', // status
            trx_status_message: 'No record', // desc
            trx_response_code: 'No record',
            trx_proxy_flag: 'No record', // trx by service type
            trx_SLA_flag: 'No record', // SLA
            trx_duration: 'No record',
            trx_complete_date: 'No record', // completed date
          };
          this.logData.push(objtmp);
        }
        this.isFetching = false;
      });
  }

  initialData() {
    this.isFetching = true;
    this.trxmonitorService
      .getAllMonToday()
      .subscribe((result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          // this.logData = [];
          // let objtmp = {
          //   created_date: 'No record',
          //   status: 'No record',
          //   trxnumber: 'No record',
          //   refnumbifast: 'No record',
          //   channel: 'No record',
          //   destbank: 'No record',
          //   srcbank: 'No record',
          //   destaccnum: 'No record',
          //   srcaccnum: 'No record',
          //   destaccname: 'No record',
          //   srcaccname: 'No record',
          //   change_who: 'No record',
          //   amount: 0,
          //   transacttype: 'No record',
          // };
          let objtmp = {
            id: 'No record',
            komi_unique_id: 'No record', // trx id
            bifast_trx_no: 'No record', // ref number
            komi_trx_no: 'No record',
            channel_type: 'No record', // chanel
            branch_code: 'No record', //
            recipient_bank: 'No record', // reciver bank code
            sender_bank: 'No record', // sender bank code
            recipient_account_no: 'No record', // receive acc num
            recipient_proxy_type: 'No record',
            recipient_proxy_alias: 'No record',
            recipient_account_name: 'No record', // seceive acc name
            sender_account_no: 'No record', // sender acc num
            sender_account_name: 'No record', // sender acc name
            charge_type: 'No record',
            trx_type: 'No record', //
            trx_amount: 'No record',
            trx_fee: 'No record',
            trx_initiation_date: 'No record', // initiation date
            trx_status_code: 'No record', // status
            trx_status_message: 'No record', // desc
            trx_response_code: 'No record',
            trx_proxy_flag: 'No record', // trx by service type
            trx_SLA_flag: 'No record', // SLA
            trx_duration: 'No record',
            trx_complete_date: 'No record', // completed date
          };
          this.logData.push(objtmp);
        } else {
          this.isdataexist = true;
          this.logData = result.data.transaction;
        }
        this.isFetching = false;
      });
  }

  maxDate() {
    const now = this.startDate;
    this.birthday = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate() + Number(this.range)
    );

    var date = this.birthday,
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);

    this.birthday = [day, mnth, date.getFullYear()].join('-');
    this.isLoggedIn = true;
    return this.birthday;
  }

  functionAmount() {
    var total = 0;
    for (var i = 0; i < this.logData.length; i++) {
      var product = this.logData[i].trx_amount;
      total += Number(product);
    }
    return total;
  }

  functionVolume() {
    return this.logData.length;
  }

  exportexcel(): void {
    let myDate = new Date();
    let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
    // this.fileName = 'xls' + datenow + '.xlsx';
    this.fileName = 'csv' + datenow + '.csv';
    /* table id is passed over here */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
  openPDF(): void {
    var doc = new jsPDF('l', 'mm', [297, 210]);
    var col = [
      [
        'Trx ID',
        'Ref. Number',
        'Trx By Servive',
        'Sender Bank Code',
        'Sender Acc Name',
        'Sender Acc Number',
        'Receiver Bank Code',
        'Receiver Acc Name',
        'Receiver Acc Number',
        'Amount',
        'Purpose of Trx',
        'Initiation Date/Time',
        'Completed Date/Time',
        'Chanel Type',
        'Description',
        'Status',
        'Status Reason Code',
        'SLA',
      ],
    ];
    var rows = [];

    let myDate = new Date();
    let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
    this.fileName = 'pdf' + datenow + '.pdf';
    this.logData.forEach((element) => {
      var tpf = '';
      var pot = 'transfer';
      var stat = '';
      var sla = '';

      if (element.trx_proxy_flag == 1) {
        tpf = 'Credit Transfer';
      } else {
        tpf = 'Credit Transfer with Proxy';
      }

      if (element.trx_status_code == 'S') {
        stat = 'Sucess';
      } else if (element.trx_status_code == 'F') {
        stat = 'Failed';
      } else if (element.trx_status_code == 'E') {
        stat = 'Exception';
      } else if (element.trx_status_code == 'P') {
        stat = 'Pending';
      } else {
        stat = 'No Record';
      }

      if (element.trx_SLA_flag == 1) {
        sla = 'Sesuai SLA';
      } else {
        sla = 'Tidak Sesuai SLA';
      }

      var temp = [
        element.komi_unique_id,
        element.bifast_trx_no,
        // element.trx_proxy_flag,
        tpf,
        element.sender_bank,
        element.sender_account_name,
        element.sender_account_no,
        element.recipient_bank,
        element.recipient_account_name,
        element.recipient_account_no,
        element.trx_amount,
        pot,
        element.trx_initiation_date,
        element.trx_complete_date,
        element.channel_type,
        element.trx_status_message,
        // element.trx_status_code,
        stat,
        element.trx_response_code,
        // element.trx_SLA_flag,
        sla,
      ];
      rows.push(temp);
    });
    // doc.autoTable(col, rows);
    autoTable(doc, {
      head: col,
      body: rows,
      didDrawCell: (rows) => {},
    });

    doc.save(this.fileName);
  }

  removeFilter() {
    // this.startDate = null;
    this.startDate = new Date();
    // this.endDate = null;
    this.endDate = null;
    this.isactivesSelected = [0];
    this.channelSelected = [0];
    this.transactiontypeSelected = [0];
    this.refreshData();
    this.isLoggedIn = false;
  }
}

export interface Customer {
  id?: number;
  name?: string;
  country?: string;
  company?: string;
  date?: string | Date;
  status?: string;
  activity?: number;
  representative?: string;
  verified?: boolean;
  balance?: boolean;
}
