import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionmonitorComponent } from './transactionmonitor.component';

describe('TransactionmonitorComponent', () => {
  let component: TransactionmonitorComponent;
  let fixture: ComponentFixture<TransactionmonitorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionmonitorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionmonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
