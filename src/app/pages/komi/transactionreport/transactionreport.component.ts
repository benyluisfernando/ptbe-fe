import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/services/auth.service';
import { DatePipe } from '@angular/common';
import { TrxreportService } from 'src/app/services/komi/trxreport.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { element } from 'protractor';

@Component({
  selector: 'app-transactionreport',
  templateUrl: './transactionreport.component.html',
  styleUrls: ['./transactionreport.component.scss'],
})
export class TransactionreportComponent implements OnInit {
  // customers: Customer[];
  rangeDates: Date[];
  isactives: any = [];
  description: string = '';
  isactivesSelected: any = {};
  channels: any = [];
  channelSelected: any = {};
  transactiontype: any = [];
  transactiontypeSelected: any = 0;
  display = false;
  displayPrv = false;
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;
  userInfo: any = {};
  tokenID: string = '';
  rptData: any[] = [];
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    public datepipe: DatePipe,
    private trxreportService: TrxreportService
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Transaction Report' }];
    // this.channels=[{label:"ATM", value:1},{label:"Mobile Banking", value:2},{label:"Web Banking", value:3}];
    // this.transactiontype = [
    //   { label: 'Incoming Report', value: 1 },
    //   { label: 'Outgoing Report', value: 2 },
    // ];
    this.transactiontype = [
      { label: 'Summary Data Incoming', value: 1 },
      { label: 'Summary Data Outgoing', value: 2 },
      //{ label: 'Summary Data Branch', value: 3 },
      { label: 'Summary Data Fee', value: 3 },
    ];
    // this.transactiontype=[{label:"Incoming Transaction", value:1},{label:"Outgoing Transaction", value:2}];
    this.rptData = [];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.initialData();
  }
  initialData() {
    this.rptData = [];
    this.isFetching = true;
    this.trxreportService
      .getAllReport()
      .subscribe((result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));

        if (result.status === 202) {
          // id, rpttype, change_who, status, description, pathfile, created_date, idtenant
          let objtmp = {
            created_date: 'No record',
            rpttype: 'No record',
            change_who: 'No record',
            status: 'No record',
          };
          this.rptData.push(objtmp);
        } else {
          this.rptData = result.data;
        }

        this.isFetching = false;
      });
  }
  refreshData() {
    this.rptData = [];
    this.isFetching = true;
    console.log('Range date : ' + this.rangeDates);
    let start_date = null;
    let end_date = null;
    let payload: any = {};
    if (this.rangeDates != undefined) {
      start_date = this.datepipe.transform(this.rangeDates[0], 'yyyy-MM-dd');
      end_date = this.datepipe.transform(this.rangeDates[1], 'yyyy-MM-dd');
      if (start_date) payload.start_date = start_date;
      if (end_date) payload.end_date = end_date;
      if (this.transactiontypeSelected) {
        payload.trxtype = this.transactiontypeSelected.value;
      } else {
        console.log('>>>>>>>>>>>>>>> KOSONGAN');
        payload.trxtype = 0;
      }
      if (this.description) payload.desc = this.description;
      console.log(JSON.stringify(payload));
      this.isFetching = true;
      this.trxreportService
        .generatReport(payload)
        .subscribe((response: BackendResponse) => {
          console.log(JSON.stringify(response));
          if (response.status == 200) {
            this.initialData();
            this.showTopSuccess(
              'Generating report, please wait or click refresh to check the report status'
            );
          }
          // this.isFetching=false;
        });
    } else {
      this.showTopError('Please fill the range date to generate data');
    }
  }

  async countAmount(dt) {
    let amountTot = 0;
    console.log(dt);
    let amountTotal = await dt[0].datacol.map((dt) => {
      console.log(dt);
      amountTot = amountTot + dt.amount;
    });
    console.log(amountTot);
    return amountTot;
  }

  async countFee(dt) {
    let amountTot = 0;
    console.log(dt);
    let amountTotal = await dt[0].datacol.map((dt) => {
      console.log(dt);
      amountTot = amountTot + dt.feeamount;
    });
    console.log(amountTot);
    return amountTot;
  }

  downloadXLS(payload, dt) {
    console.log(dt);
    console.log(payload);
    this.trxreportService
      .downloadReport(payload)
      .subscribe(async (response: BackendResponse) => {
        console.log(response);
        if (response.status == 200) {
          // let workbook = new Workbook();
          // let worksheet = workbook.addWorksheet('Report Transaction');
          // {"status":200,"data":[{"headercol":{"colhead":["Status","Trx Number","Reff BiFast","channel","destbank","srcbank","destaccnum","srcaccnum","destaccname","srcaccname","created_date","change_who","amount","transacttype","idtenant"]},"datacol":[{"id":"2","status":"ACTC","trxnumber":"2233443334","refnumbifast":"992233443334","channel":"ATM","destbank":"564","srcbank":"008","destaccnum":"3524534533454","srcaccnum":"799889998999","destaccname":"Peter Pan","srcaccname":"Rita Gunato","created_date":"2021-09-25 10:09:39","change_who":"SYS","last_updated_date":null,"amount":500000000,"transacttype":"Incoming","idtenant":10},{"id":"1","status":"RJCT","trxnumber":"2233443333","refnumbifast":"992233443333","channel":"Mobile Banking","destbank":"014","srcbank":"564","destaccnum":"899889998999","srcaccnum":"3524534533453","destaccname":"Udin Petor","srcaccname":"Peter Pan","created_date":"2021-09-25 10:09:14","change_who":"SYS","last_updated_date":null,"amount":100000000,"transacttype":"Outgoing","idtenant":10}],"created_date":"2021-09-28T05:38:17.662Z","srcpayload":{"start_date":"2021-09-23","end_date":"2021-09-30","trxtype":0,"desc":"ALL 1","iddata":"40","idtenant":"10"},"id":"2","filename":"20210928123817"}]}
          if (response.data.length > 0) {
            let allrecord = response.data[0];
            let workbook = new Workbook();
            let worksheet = workbook.addWorksheet('Report Transaction');
            worksheet.mergeCells('C1', 'F4');
            let titleRow = worksheet.getCell('C1');
            titleRow.value = 'Komi trafic Report';
            titleRow.font = {
              name: 'Calibri',
              size: 14,
              underline: 'single',
              bold: true,
              color: { argb: '0085A3' },
            };
            titleRow.alignment = { vertical: 'middle', horizontal: 'center' };
            worksheet.addRow([]);

            let totalTransaction = worksheet.getCell('I2');
            totalTransaction.value = 'Total Transaction';
            totalTransaction.font = {
              name: 'Calibri',
              size: 11,
              //underline: 'single',
              //bold: true,
              //color: { argb: '0085A3' },
            };
            totalTransaction.alignment = {
              vertical: 'middle',
              horizontal: 'center',
            };
            worksheet.addRow([]);

            let totalTransactionVal = worksheet.getCell('J2');
            totalTransactionVal.value = response.data.length + 1;
            totalTransactionVal.font = {
              name: 'Calibri',
              size: 11,
              //underline: 'single',
              //bold: true,
              //color: { argb: '0085A3' },
            };
            totalTransactionVal.alignment = {
              vertical: 'middle',
              horizontal: 'center',
            };
            worksheet.addRow([]);

            let totalAmount = worksheet.getCell('I3');
            totalAmount.value = 'Total Amount';
            totalAmount.font = {
              name: 'Calibri',
              size: 11,
              //underline: 'single',
              //bold: true,
              //color: { argb: '0085A3' },
            };
            totalAmount.alignment = {
              vertical: 'middle',
              horizontal: 'center',
            };
            worksheet.addRow([]);

            let totalAmountVal = worksheet.getCell('J3');
            totalAmountVal.value = await this.countAmount(response.data);
            totalAmountVal.font = {
              name: 'Calibri',
              size: 11,
              //underline: 'single',
              //bold: true,
              //color: { argb: '0085A3' },
            };
            totalAmountVal.alignment = {
              vertical: 'middle',
              horizontal: 'center',
            };
            worksheet.addRow([]);

            if (dt.rpttype == 3) {
              let totalFee = worksheet.getCell('I4');
              totalFee.value = 'Total Fee';
              totalFee.font = {
                name: 'Calibri',
                size: 11,
                //underline: 'single',
                //bold: true,
                //color: { argb: '0085A3' },
              };
              totalFee.alignment = {
                vertical: 'middle',
                horizontal: 'center',
              };
              worksheet.addRow([]);

              let totalFeeVal = worksheet.getCell('J4');
              totalFeeVal.value = await this.countFee(response.data);
              totalFeeVal.font = {
                name: 'Calibri',
                size: 11,
                //underline: 'single',
                //bold: true,
                //color: { argb: '0085A3' },
              };
              totalFeeVal.alignment = {
                vertical: 'middle',
                horizontal: 'center',
              };
              worksheet.addRow([]);
            }

            //Adding Header Row
            // console.log("Header "+JSON.stringify(allrecord.headercol.colhead));
            let headerRow = worksheet.addRow(allrecord.headercol.colhead);
            headerRow.eachCell((cell, number) => {
              cell.fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: '4167B8' },
                bgColor: { argb: '' },
              };
              cell.font = {
                bold: true,
                color: { argb: 'FFFFFF' },
                size: 12,
              };
            });
            let datarows = allrecord.datacol;
            console.log('Data ' + JSON.stringify(allrecord.datacol));
            // ["Status","Trx Number","Reff BiFast","channel","destbank","srcbank","destaccnum","srcaccnum","destaccname","srcaccname","created_date","change_who","amount","transacttype","idtenant"]

            // Adding Data with Conditional Formatting
            // {"id":"2","status":"ACTC","trxnumber":"2233443334","refnumbifast":"992233443334","channel":"ATM","destbank":"564","srcbank":"008","destaccnum":"3524534533454","srcaccnum":"799889998999","destaccname":"Peter Pan","srcaccname":"Rita Gunato","created_date":"2021-09-25 10:09:39","change_who":"SYS","last_updated_date":null,"amount":500000000,"transacttype":"Incoming","idtenant":10}
            datarows.forEach((d) => {
              let datobj = [
                d.status,
                d.trxnumber,
                d.refnumbifast,
                d.channel,
                d.destbank,
                d.srcbank,
                d.destaccnum,
                d.srcaccnum,
                d.destaccname,
                d.srcaccname,
                d.created_date,
                d.change_who,
                d.amount,
                d.transacttype,
                d.idtenant,
              ];
              let row = worksheet.addRow(datobj);
            });
            //Generate & Save Excel File
            let myDate = new Date();
            let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
            let fileName = 'xl' + datenow + '.xlsx';

            workbook.xlsx.writeBuffer().then((data) => {
              let blob = new Blob([data], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
              });
              fs.saveAs(blob, fileName);
            });
          } else {
            this.showTopError('No data on the report');
          }
        }
      });
  }

  async downloadPDF(payload) {
    console.log(payload);
    await this.trxreportService
      .downloadReport(payload)
      .subscribe(async (response: BackendResponse) => {
        // console.log(JSON.stringify(response));
        if (response.status == 200) {
          if (response.data.length > 0) {
            let totalAmount = await this.countAmount(response.data);
            let totalTransaction = response.data[0].datacol.length;
            let allrecord = response.data[0];
            var doc = new jsPDF('l', 'mm', [297, 210]);
            doc.text(`Total Transaction: ${totalTransaction}`, 10, 20);
            doc.text(`Total Amount: ${totalAmount}`, 10, 30);
            var col = [
              [
                'Trx Date',
                'Status',
                'TrxNum',
                'Reff BiFAST',
                'Channel',
                'Dest Bank',
                'Src Bank',
                'Dest AccNum',
                'Src AccNum',
                'Dest Acc Name',
                'Src Acc Name',
                'Amount',
              ],
            ];
            var rows = [];
            let myDate = new Date();
            let datenow = this.datepipe.transform(myDate, 'yyyyMMddHHmmss');
            let fileName = 'pdf' + datenow + '.pdf';
            let datarows = allrecord.datacol;
            datarows.forEach((element) => {
              var temp = [
                element.created_date,
                element.status,
                element.trxnumber,
                element.refnumbifast,
                element.channel,
                element.destbank,
                element.srcbank,
                element.destaccnum,
                element.srcaccnum,
                element.destaccname,
                element.srcaccname,
                element.amount,
              ];
              rows.push(temp);
            });
            autoTable(doc, {
              head: col,
              body: rows,
              didDrawCell: (rows) => {},
            });
            doc.save(fileName);
          } else {
            this.showTopError('No data on the report');
          }
        }
      });
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Generated Report',
      detail: message,
    });
  }
  showTopError(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }
}
