import { Component, OnInit } from '@angular/core';

import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';
import { EventlogService } from 'src/app/services/komi/eventlog.service';

@Component({
  selector: 'app-systemlog',
  templateUrl: './systemlog.component.html',
  styleUrls: ['./systemlog.component.scss'],
})
export class SystemlogComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedProxy: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';
  limitData: any[] = [];
  prxheader: any = [
    { label: 'Usename', sort: 'userid' },
    { label: 'Event', sort: 'value' },
    { label: 'Description', sort: 'description' },
    { label: 'Date Time', sort: 'formateddate' },
  ];
  prxcolname: any = ['userid', 'value', 'description', 'formateddate'];
  prxcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  prxcolwidth: any = ['', '', '', ''];
  prxcollinghref: any = { url: '#', label: 'Application' };
  limitactionbtn: any = [0, 0, 0, 0, 0];
  prxaddbtn = { route: 'detail', label: 'Add Data' };
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private eventService: EventlogService
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Event Log' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();
  }
  refreshingProxy() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh Limit ');
    this.eventService
      .getSystemLog()
      .subscribe(async (result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.limitData = [];

          let objtmp = {
            value: 'No records',
            description: 'No records',
            formateddate: 'No records',
            userid: 'No records',
          };
          this.limitactionbtn = [0, 0, 0, 0, 0];
          this.limitData.push(objtmp);
        } else {
          this.limitData = result.data;
        }
        // if(result.data.userGroup.length > 0) {
        //   this.proxyData = [];
        //   this.proxyData = result.data.userGroup;
        // } else {
        //   this.proxyData = [];
        //   let objtmp = {"groupname":"No records"};
        //   this.proxyData.push(objtmp);
        // }
        this.isFetching = false;
      });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedProxy = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedProxy = data;
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
