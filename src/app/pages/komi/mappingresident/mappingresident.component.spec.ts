import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingresidentComponent } from './mappingresident.component';

describe('MappingresidentComponent', () => {
  let component: MappingresidentComponent;
  let fixture: ComponentFixture<MappingresidentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MappingresidentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingresidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
