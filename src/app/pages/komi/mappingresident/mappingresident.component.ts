import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { ResidentstatusService } from 'src/app/services/komi/residentstatus.service';

@Component({
  selector: 'app-mappingresident',
  templateUrl: './mappingresident.component.html',
  styleUrls: ['./mappingresident.component.scss'],
})
export class MappingresidentComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedProxy: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';
  proxyData: any[] = [];
  prxheader: any = [
    { label: 'Resident Status Code', sort: 'code' },
    { label: 'Resident Status Name', sort: 'name' },
    { label: 'Resident Status (CB)', sort: 'status_cb' },
    { label: 'Resident Status (BI-FAST)', sort: 'status_bf' },
    { label: 'Status', sort: 'status' },
  ];
  prxcolname: any = ['code', 'name', 'status_cb', 'status_bf', 'status'];
  prxcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  prxcolwidth: any = ['', '', '', '', '', '', '', '', ''];
  prxcollinghref: any = { url: '#', label: 'Application' };
  residentactiontbn: any = [1, 1, 1, 1, 0];
  prxaddbtn = { route: 'detail', label: 'Add Data' };
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private residentService: ResidentstatusService
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Resident Status' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();
  }
  refreshingProxy() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh Resident ');
    this.residentService.getAll().subscribe(async (result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if (result.status === 202) {
        this.proxyData = [];

        let objtmp = {
          code: 'No records',
          name: 'No records',
          status_cb: 'No records',
          status_bf: 'No records',
          status: 'No records',
        };
        this.residentactiontbn = [1, 0, 0, 0, 0];
        this.proxyData.push(objtmp);
      } else {
        if (result.data.resident.length > 0) {
          await result.data.resident.map((dt) => {
            dt.status =
              dt.status == 1
                ? 'Active'
                : dt.status == 2
                ? 'InActive'
                : 'InActive';
          });
          this.proxyData = result.data.resident;
        }
      }
      // if(result.data.userGroup.length > 0) {
      //   this.proxyData = [];
      //   this.proxyData = result.data.userGroup;
      // } else {
      //   this.proxyData = [];
      //   let objtmp = {"groupname":"No records"};
      //   this.proxyData.push(objtmp);
      // }
      this.isFetching = false;
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedProxy = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedProxy = data;
  }

  deleteResident() {
    console.log(this.selectedProxy);
    this.residentService
      .delete(this.selectedProxy.id)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingProxy();
      });
    // this.proxyadminService.deleteGroup(payload).subscribe((resp: BackendResponse)=>{
    //   console.log(">> hasil Delete "+ resp);
    //       if (resp.status === 200) {
    //       this.showTopSuccess(resp.data);
    //     }
    //   this.display = false;
    //   this.refreshingBic();
    // });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
