import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, SelectItem } from 'primeng/api';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';
import { ResidentstatusService } from 'src/app/services/komi/residentstatus.service';

@Component({
  selector: 'app-mappingresidentdetail',
  templateUrl: './mappingresidentdetail.component.html',
  styleUrls: ['./mappingresidentdetail.component.scss'],
})
export class MappingresidentdetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  proxytypes: any = [];
  proxystates: any = [];
  registrarbics: any = [];
  idtypes: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  items: SelectItem[];
  item: string;
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private proxyadminService: ProxyadminService,
    private bussinesparamService: BussinesparamService,
    private bicadminService: BicadminService,
    private residentService: ResidentstatusService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      {
        label: 'Resident Status',
        command: (event) => {
          this.location.back();
        },
        url: '',
      },
      { label: this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // registration_id, proxy_type, proxy_value, registrar_bic, account_number, account_type, account_name, identification_number_type, identification_number_value, proxy_status
      this.groupForm = this.formBuilder.group({
        code: ['', Validators.required],
        name: ['', Validators.required],
        status_cb: ['', Validators.required],
        status_bf: ['', Validators.required],
        status: ['', Validators.required],
      });

      this.proxytypes = [];
      this.idtypes = [];
      this.registrarbics = [];
      this.bussinesparamService
        .getAllByCategory('KOMI_LMT_STS')
        .subscribe((result: BackendResponse) => {
          console.log('>>>>>>> ' + JSON.stringify(result));
          if (result.status === 200) {
            this.proxytypes = result.data;
          } else {
            // this.proxyData = result.data.bicAdmins;
          }
        });

      if (this.isEdit) {
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.residentService
            .getById(this.userId)
            .subscribe(async (result: BackendResponse) => {
              //console.log('Data edit bic ' + JSON.stringify(result.data));
              // if (this.isOrganization) {
              this.groupForm.patchValue({
                code: result.data.resident.code,
                name: result.data.resident.name,
                status_cb: result.data.resident.status_cb,
                status_bf: result.data.resident.status_bf,
                status: result.data.resident.status,
              });
              // console.log(this.user);
            });
        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      // bank_code, bic_code, bank_name
      if (!this.isEdit) {
        payload = {
          code: this.groupForm.get('code')?.value,
          name: this.groupForm.get('name')?.value,
          status_cb: this.groupForm.get('status_cb')?.value,
          status_bf: this.groupForm.get('status_bf')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.residentService
          .insert(payload)
          .subscribe((result: BackendResponse) => {
            if (result.status == 200) {
              this.location.back();
            }
            console.log('>>>>>>>> payload ' + JSON.stringify(result));
            //this.location.back();
          });
      } else {
        payload = {
          id: this.userId,
          code: this.groupForm.get('code')?.value,
          name: this.groupForm.get('name')?.value,
          status_cb: this.groupForm.get('status_cb')?.value,
          status_bf: this.groupForm.get('status_bf')?.value,
          status: this.groupForm.get('status')?.value,
        };
        console.log('>>>>>>>> payload ' + JSON.stringify(payload));
        this.residentService
          .update(payload)
          .subscribe((result: BackendResponse) => {
            // console.log(">>>>>>>> return "+JSON.stringify(result));
            if (result.status == 200) {
              this.location.back();
            }
          });
        //this.location.back();
      }
    }

    console.log(this.groupForm.valid);
  }
}
