import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkmanageComponent } from './networkmanage.component';

describe('NetworkmanageComponent', () => {
  let component: NetworkmanageComponent;
  let fixture: ComponentFixture<NetworkmanageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkmanageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkmanageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
