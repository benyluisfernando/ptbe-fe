import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AuthService } from 'src/app/services/auth.service';
import { NetworkmanagementService } from 'src/app/services/komi/networkmanagement.service';

@Component({
  selector: 'app-networkmanage',
  templateUrl: './networkmanage.component.html',
  styleUrls: ['./networkmanage.component.scss']
})
export class NetworkmanageComponent implements OnInit {
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  userInfo:any = {};
  tokenID:string = "";
  constructor(private authservice:AuthService,public dialogService: DialogService,
    public messageService: MessageService, private networkmanagementService: NetworkmanagementService) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Network Management Request' }
    ];
    this.authservice.whoAmi().subscribe((value) =>{
      // console.log(">>> User Info : "+JSON.stringify(value));
        this.userInfo = value.data;
        this.tokenID = value.tokenId; 
    });
  }

  signOn(){
    console.log(">>>> Sign On Clicked");
  }

  signOff(){
    console.log(">>>> Sign Off Clicked");
  }

  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
