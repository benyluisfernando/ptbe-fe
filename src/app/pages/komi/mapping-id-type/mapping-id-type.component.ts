import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { IdentityTypeService } from 'src/app/services/komi/identity-type.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';

@Component({
  selector: 'app-mapping-id-type',
  templateUrl: './mapping-id-type.component.html',
  styleUrls: ['./mapping-id-type.component.scss'],
})
export class MappingIdTypeComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedProxy: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';
  proxyData: any[] = [];
  prxheader: any = [
    { label: 'Identification Type Code', sort: 'code' },
    { label: 'Identification Type Name', sort: 'name' },
    { label: 'Identification Type (CB)', sort: 'type_cb' },
    { label: 'Identification Type (BI-FAST)', sort: 'type_bf' },
    { label: 'Status', sort: 'status' },
  ];
  prxcolname: any = ['code', 'name', 'type_cb', 'type_bf', 'status'];
  prxcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  prxcolwidth: any = ['', '', '', '', '', '', '', '', ''];
  prxcollinghref: any = { url: '#', label: 'Application' };
  identityactionbtn: any = [1, 1, 1, 1, 0];
  prxaddbtn = { route: 'detail', label: 'Add Data' };
  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private idTypeService: IdentityTypeService
  ) {}

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Identifiaction Type' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();
  }
  refreshingProxy() {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh Identity ');
    this.idTypeService.getAll().subscribe(async (result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if (result.status === 202) {
        this.proxyData = [];

        let objtmp = {
          code: 'No records',
          name: 'No records',
          type_cb: 'No records',
          type_bf: 'No records',
          status: 'No records',
        };
        this.identityactionbtn = [1, 0, 0, 0, 0];
        this.proxyData.push(objtmp);
      } else {
        if (result.data.idType.length > 0) {
          await result.data.idType.map((dt) => {
            dt.status =
              dt.status == 1
                ? 'Active'
                : dt.status == 2
                ? 'InActive'
                : 'InActive';
          });
          this.proxyData = result.data.idType;
        }
      }
      // if(result.data.userGroup.length > 0) {
      //   this.proxyData = [];
      //   this.proxyData = result.data.userGroup;
      // } else {
      //   this.proxyData = [];
      //   let objtmp = {"groupname":"No records"};
      //   this.proxyData.push(objtmp);
      // }
      this.isFetching = false;
    });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedProxy = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedProxy = data;
  }

  deleteIdentity() {
    console.log(this.selectedProxy);
    this.idTypeService
      .delete(this.selectedProxy.id)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingProxy();
      });
    // this.proxyadminService.deleteGroup(payload).subscribe((resp: BackendResponse)=>{
    //   console.log(">> hasil Delete "+ resp);
    //       if (resp.status === 200) {
    //       this.showTopSuccess(resp.data);
    //     }
    //   this.display = false;
    //   this.refreshingBic();
    // });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
