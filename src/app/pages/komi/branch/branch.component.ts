import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BranchService } from 'src/app/services/komi/branch.service';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.scss']
})
export class BranchComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedbranch: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  userInfo:any = {};
  tokenID:string = "";
  branchData: any[] = [];
  branchheader:any = [{'label':'City Code', 'sort':'citycode'},{'label':'City Name', 'sort': 'cityname'}, {'label':'Branch Code', 'sort': 'branchcode'}, {'label':'Branch Name', 'sort': 'branchname'}, {'label':'Active', 'sort': 'status'}];
  branchcolname:any = ["citycode", "cityname", "branchcode", "branchname", "status"]
  branchcolhalign:any = ["","","","","p-text-center"]
  branchcolwidth:any = ["","","","",""];
  branchcollinghref:any = {'url':'#','label':'Application'}
  branchactionbtn:any = [1,1,1,1,0]
  branchaddbtn ={'route':'detail', 'label':'Add Data'}
  constructor(
    private authservice:AuthService,
    public dialogService: DialogService,
    public messageService: MessageService, 
    private branchService: BranchService) { }
    // private sysparamService:sysparamService

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Branch Management' }
    ];
    this.authservice.whoAmi().subscribe((value) =>{
      // console.log(">>> User Info : "+JSON.stringify(value));
        this.userInfo = value.data;
        this.tokenID = value.tokenId; 
    });
    this.refreshingBranch();
  }
  refreshingBranch() {
    this.isFetching= true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      console.log(">>>>>>> Refresh sysParam ");
    this.branchService.getAllBranchByTenant().subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if(result.status === 202) {
          this.branchData = [];
          let objtmp = {"citycode":"No records","cityname":"No records","branchcode":"No records","branchname":"No records","status":"No records"};
          this.branchData.push(objtmp);
      } else {
        this.branchData = result.data.Branchs;
      }
     
      this.isFetching=false;
    });
  }
  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.display = true;
    this.selectedbranch = data;
  }

  previewConfirmation(data:any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.displayPrv = true;
    this.selectedbranch = data;
  }

  deleteBranch() {
    console.log(this.selectedbranch);
    let Branch = this.selectedbranch;
    const payload = {Branch};
    console.log(">> Di delete "+JSON.stringify(payload.Branch))
    this.branchService.deleteBranchByTenant(payload.Branch.id).subscribe((resp: BackendResponse)=>{
        this.display = false;
        this.showTopSuccess(resp.data);
       this.refreshingBranch();
    });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
