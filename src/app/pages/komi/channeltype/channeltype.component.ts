import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { ChanneltypeService } from 'src/app/services/komi/channeltype.service';


@Component({
  selector: 'app-channeltype',
  templateUrl: './channeltype.component.html',
  styleUrls: ['./channeltype.component.scss']
})
export class ChanneltypeComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedchn: any = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  isFetching:boolean = false;
  userInfo:any = {};
  tokenID:string = "";
  chnData: any[] = [];
  // channeltype, status created_date
  chnheader:any = [{'label':'Channel Code', 'sort':'bank_code'}, {'label':'Channel Type', 'sort': 'bank_name'},{'label':'Active', 'sort': 'status'}, {'label':'Created At', 'sort': 'created_date'}];
  chncolname:any = ["channelcode", "channeltype", "status", "created_date"]
  chncolhalign:any = ["p-text-center","","p-text-center","p-text-center",]
  // chncolwidth:any = [{'width':'170px'},{'width':'170px'},"",{'width':'170px'}];
  chncolwidth:any = [{'width':'170px'},"",{'width':'170px'},{'width':'170px'}];
  chncollinghref:any = {'url':'#','label':'Application'}
  chnactionbtn:any = [1,1,1,1,0]
  chnaddbtn ={'route':'detail', 'label':'Add Data'}
  constructor(private authservice:AuthService,public dialogService: DialogService,
    public messageService: MessageService,private channeltypeService:ChanneltypeService ) { }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Channel Type' }
    ];
    this.authservice.whoAmi().subscribe((value) =>{
      // console.log(">>> User Info : "+JSON.stringify(value));
        this.userInfo = value.data;
        this.tokenID = value.tokenId; 
    });
    this.refreshingChn();
  }
  refreshingChn() {
    this.isFetching= true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      console.log(">>>>>>> Refresh Chn ");
    this.channeltypeService.getAllChnByTenant().subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
    // var query = "SELECT id, channelcode, channeltype, status, TO_CHAR(created_date,'yyyy-MM-dd HH:mm:ss') created_date, change_who, last_update_date, idtenant FROM public.m_channeltype WHERE idtenant=$1 order by created_date desc";

      if(result.status === 202) {
          this.chnData = [];
          let objtmp = {"channelcode":"No records","channeltype":"No records", "status":"No records", "created_date" : "No records"};
          this.chnData.push(objtmp);
      } else {
        this.chnData = result.data;
      }
      this.isFetching=false;
    });
  }
  deleteConfirmation(data: any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.display = true;
    this.selectedchn = data;
  }

  previewConfirmation(data:any) {
    console.log("Di Emit nih dari child "+JSON.stringify(data));
    this.displayPrv = true;
    this.selectedchn = data;
  }


  deleteChn() {
    console.log(this.selectedchn);
    let chn = this.selectedchn;
    const payload = {chn};
    console.log(">> Di delete "+JSON.stringify(payload.chn))
    this.channeltypeService.deleteChnByTenant(payload.chn.id).subscribe((resp: BackendResponse)=>{
        this.display = false;
        this.showTopSuccess(resp.data);
      this.refreshingChn();
    });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }
}
