import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuItem } from 'primeng/api';
import {Location} from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { ChanneltypeService } from 'src/app/services/komi/channeltype.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';

@Component({
  selector: 'app-channeltypedetail',
  templateUrl: './channeltypedetail.component.html',
  styleUrls: ['./channeltypedetail.component.scss']
})
export class ChanneltypedetailComponent implements OnInit {
  extraInfo:any = {};
  isEdit:boolean= false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo:any = {};
  selectedApps: any = [];
  tokenID:string = "";
  groupForm!: FormGroup;
  old_code:any = "";
  submitted = false;
  constructor(private router: Router,private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location, private channeltypeService:ChanneltypeService) {this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
      let checkurl = this.extraInfo.indexOf("%23") !== -1?true:false;
      console.log(">>>>>>>>>>> "+this.extraInfo);
      console.log(checkurl);
      if(checkurl) this.isEdit=true; }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Channel Type',command: (event) => {
        this.location.back();
    }, url:"" }, { label: this.isEdit ?'Edit data':'Add data'}
    ];
    this.stateOptions = [
      { label: 'Active', value: 1 },
      { label: 'Inactive', value: 0 },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // "channelcode", "channeltype", "status"
      this.groupForm = this.formBuilder.group({
        channelcode: ['', Validators.required],
        channeltype: ['', Validators.required],
        isactive: [0],
      });

      if(this.isEdit){ 
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          this.channeltypeService.getChnById(this.userId).subscribe(async (result: BackendResponse) => {
            console.log("Data edit chn "+JSON.stringify(result.data));
            // if (this.isOrganization) {
              this.groupForm.patchValue({
                channelcode: result.data.channelcode,
                channeltype: result.data.channeltype,
                isactive: result.data.status,
              });
              this.old_code=result.data.id;
              // console.log(this.user);
          });

        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;  
      // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
      // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
      if (this.groupForm.valid) {
        // event?.preventDefault;
        // var groupacl = this.groupForm.get('orgobj')?.value;
        let payload ={};
        // bank_code, bic_code, bank_name
        if(!this.isEdit) {
          payload = {
            channelcode: this.groupForm.get('channelcode')?.value,
            channeltype: this.groupForm.get('channeltype')?.value,
            status: this.groupForm.get('isactive')?.value
          };
          console.log(">>>>>>>> payload "+JSON.stringify(payload));
          this.channeltypeService.insertChnByTenant(payload).subscribe((result: BackendResponse) => {
              // if (result.status === 200) {
              //   this.location.back();
              // }
              console.log(">>>>>>>> payload "+JSON.stringify(result));
              this.location.back();
          });
        } else {
          payload = {
            channelcode: this.groupForm.get('channelcode')?.value,
            channeltype: this.groupForm.get('channeltype')?.value,
            status: this.groupForm.get('isactive')?.value,
            id:this.old_code
          };
          this.channeltypeService.updateChnByTenant(payload).subscribe((result: BackendResponse) => {
            // if (result.status === 200) {
            //   this.location.back();
            // }
            console.log(">>>>>>>> payload "+JSON.stringify(result));
            this.location.back();
        });
          // payload = {
          //   fullname: this.groupForm.get('fullname')?.value,
          //   userid: this.userId,
          //   group: this.groupForm.get('orgMlt')?.value,
          //   isactive: this.groupForm.get('isactive')?.value,
          // }
          // console.log(">>>>>>>> payload "+JSON.stringify(payload));
          // this.umService
          // .updatebyAdmin(payload)
          // .subscribe((result: BackendResponse) => {
          //   // console.log(">>>>>>>> return "+JSON.stringify(result));
          //   if (result.status === 200) {
          //     this.location.back();
          //   }
          // });
          // this.location.back();
        }
        
      }
    

    console.log(this.groupForm.valid);
  }
}
