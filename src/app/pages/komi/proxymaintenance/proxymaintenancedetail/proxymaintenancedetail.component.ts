import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, SelectItem } from 'primeng/api';
import {Location} from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';

@Component({
  selector: 'app-proxymaintenancedetail',
  templateUrl: './proxymaintenancedetail.component.html',
  styleUrls: ['./proxymaintenancedetail.component.scss']
})
export class ProxymaintenancedetailComponent implements OnInit {
  extraInfo:any = {};
  isEdit:boolean= false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo:any = {};
  selectedApps: any = [];
  proxytypes: any = [];
  proxystates: any = [];
  registrarbics: any = [];
  idtypes:any = [];
  tokenID:string = "";
  groupForm!: FormGroup;
  items: SelectItem[];
    item: string;
  submitted = false;
  constructor(private router: Router,private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location, private proxyadminService:ProxyadminService, private bussinesparamService:BussinesparamService, private bicadminService:BicadminService) { 
      this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
      let checkurl = this.extraInfo.indexOf("%23") !== -1?true:false;
      console.log(">>>>>>>>>>> "+this.extraInfo);
      console.log(checkurl);
      if(checkurl) this.isEdit=true;
    }

  ngOnInit(): void {
    this.home = {icon: 'pi pi-home', routerLink: '/mgm/home'};
    this.breadcrumbs = [
      { label: 'Proxy Maintenance',command: (event) => {
        this.location.back();
    }, url:"" }, { label: this.isEdit ?'Edit data':'Add data'}
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // registration_id, proxy_type, proxy_value, registrar_bic, account_number, account_type, account_name, identification_number_type, identification_number_value, proxy_status
      this.groupForm = this.formBuilder.group({
        proxy_type: ['', Validators.required],
        proxy_value: ['', Validators.required],
        registrar_bic: ['', Validators.required],
        account_number: ['', Validators.required],
        account_type: ['', Validators.required],
        account_name: [''],
        identification_number_type: ['', Validators.required],
        identification_number_value: ['', Validators.required],
        proxy_status: [''],
      });


      this.proxytypes=[];
    this.idtypes=[];
    this.registrarbics=[];

    this.bicadminService.getAllBicByTenant().subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if(result.status === 200) {
        this.registrarbics=result.data.bicAdmins;
      } else {
        // this.proxyData = result.data.bicAdmins;
      }
    });

    this.bussinesparamService.getAllByCategory("KOMI_PRX_TYP").subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if(result.status === 200) {
        this.proxytypes=result.data;
      } else {
        // this.proxyData = result.data.bicAdmins;
      }
    });
    this.bussinesparamService.getAllByCategory("ID_TYPE").subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if(result.status === 200) {
        this.idtypes=result.data;
      } else {
        // this.proxyData = result.data.bicAdmins;
      }
    });
    this.bussinesparamService.getAllByCategory("KOMI_PRX_STS").subscribe((result: BackendResponse) => {
      console.log('>>>>>>> ' + JSON.stringify(result));
      if(result.status === 200) {
        this.proxystates=result.data;
      } else {
        // this.proxyData = result.data.bicAdmins;
      }
    });




      if(this.isEdit){ 
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
          this.userId = this.activatedRoute.snapshot.paramMap.get('id');
          // this.bicadminService.getBicById(this.userId).subscribe(async (result: BackendResponse) => {
          //   console.log("Data edit bic "+JSON.stringify(result.data));
          //   // if (this.isOrganization) {
          //     this.groupForm.patchValue({
          //       bank_code: result.data.bicAdmins.bank_code,
          //       bic_code: result.data.bicAdmins.bic_code,
          //       bank_name: result.data.bicAdmins.bank_name,
          //     });
          //     // console.log(this.user);
          // });

        }
      }
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;  
      // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
      // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
      if (this.groupForm.valid) {
        // event?.preventDefault;
        // var groupacl = this.groupForm.get('orgobj')?.value;
        let payload ={};
        // bank_code, bic_code, bank_name
        if(!this.isEdit) {
          payload = {
            bank_code: this.groupForm.get('bank_code')?.value,
            bic_code: this.groupForm.get('bic_code')?.value,
            bank_name: this.groupForm.get('bank_name')?.value
          };
          console.log(">>>>>>>> payload "+JSON.stringify(payload));
          // this.bicadminService.insertBicByTenant(payload).subscribe((result: BackendResponse) => {
          //     // if (result.status === 200) {
          //     //   this.location.back();
          //     // }
          //     console.log(">>>>>>>> payload "+JSON.stringify(result));
          //     this.location.back();
          // });
        } else {
          // payload = {
          //   fullname: this.groupForm.get('fullname')?.value,
          //   userid: this.userId,
          //   group: this.groupForm.get('orgMlt')?.value,
          //   isactive: this.groupForm.get('isactive')?.value,
          // }
          // console.log(">>>>>>>> payload "+JSON.stringify(payload));
          // this.umService
          // .updatebyAdmin(payload)
          // .subscribe((result: BackendResponse) => {
          //   // console.log(">>>>>>>> return "+JSON.stringify(result));
          //   if (result.status === 200) {
          //     this.location.back();
          //   }
          // });
          // this.location.back();
        }
        
      }
    

    console.log(this.groupForm.valid);
  }
}
