import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxymaintenanceComponent } from './proxymaintenance.component';

describe('ProxymaintenanceComponent', () => {
  let component: ProxymaintenanceComponent;
  let fixture: ComponentFixture<ProxymaintenanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProxymaintenanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProxymaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
