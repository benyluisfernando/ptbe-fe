import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { SmtpconfigService } from 'src/app/services/komi/smtpconfig.service';

@Component({
  selector: 'app-smtpconfig',
  templateUrl: './smtpconfig.component.html',
  styleUrls: ['./smtpconfig.component.scss']
})
export class SmtpconfigComponent implements OnInit {
  display = false;
  selectedUser: any = {};
  isGroup = false;
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  smtplist: any[] = [];
  isFetching: boolean = false;
  userInfo: any = {};
  tokenID: string = '';
  secureOption: any[] = [];
//################################
idsendername:any=0;
sendername:any ="";
idusername:any=0;
username:any="";
idpwdemail:any=0;
pwdemail:any="";
idsmtphost:any=0;
smtphost:any="";
idport:any=0;
port:any=0;
idsecure:any=0;
secure:any=false;

  constructor(private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    //private userService: UsermanagerService
    public smtpconfigService: SmtpconfigService) { }

  ngOnInit(): void {
    this.secureOption = [
      { label: 'SSL (Secure)', value: true },
      { label: 'TLS (Use for GMail)', value: false }
    ];
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Smtp Config' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingConfig();
  }
  refreshingConfig() {
    this.isFetching = true;
    this.authservice.whoAmi().subscribe((data: BackendResponse) => {
      // console.log(">>>>>>> "+JSON.stringify(data));
      if ((data.status = 200)) {
        this.smtpconfigService.retriveSmtpConfig().subscribe((smtpll: BackendResponse) => {
          console.log('>>>>>>> ' + JSON.stringify(smtpll));
          this.smtplist = smtpll.data;
          // if (this.userlist.length < 1) {
          //   let objtmp = {
          //     fullname: 'No records',
          //     userid: 'No records',
          //     active: 'No records',
          //     groupname: 'No records',
          //   };
          //   this.userlist = [];
          //   this.userlist.push(objtmp);
          // }
          console.log('>>>>>>> Retrive Config ' + JSON.stringify(this.smtplist));


          this.smtplist.map((jsobj) => {
            // menusTmp.push(jsobj);
            if(jsobj.param_name === 'EMAIL_SENDERNAME') {
              this.idsendername = jsobj.id;
              this.sendername = jsobj.param_value;
          }
          if(jsobj.param_name === 'EMAIL_USER') {
            this.idusername = jsobj.id;
            this.username = jsobj.param_value;
          }
          if(jsobj.param_name === 'EMAIL_PASS') {
            this.idpwdemail = jsobj.id;
            this.pwdemail = jsobj.param_value;
          }
            if(jsobj.param_name === 'EMAIL_HOST') {
                this.idsmtphost = jsobj.id;
                this.smtphost = jsobj.param_value;
            }
            if(jsobj.param_name === 'EMAIL_PORT') {
              this.idport = jsobj.id;
              this.port = jsobj.param_value;
          }
          if(jsobj.param_name === 'EMAIL_SECURE') {
            this.idsecure = jsobj.id;
            this.secure = jsobj.param_value === 'true'?true:false;
            
            // this.secureOption = [
            //   { label: 'SSL (Secure)', value: true },
            //   { label: 'TLS (Use for GMail)', value: false }
            // ];
          }
          })
          // idsecure:any=0;
          // secure:any=false;

          this.isFetching = false;
        });
      }
    });
  }
  
  updateSender(){
    let payload = {id:this.idsendername, value:this.sendername};
    // console.log(JSON.stringify(payload));
    this.smtpconfigService.updateSmtpConfig(payload).subscribe((result:BackendResponse)=>{
      if(result.status===200){
        this.messageService.add({key: 'bc', severity:'success', summary: 'Success', detail: 'Sender Name saved!'});
      } else {
        this.messageService.add({key: 'bc', severity:'error', summary: 'Error', detail: 'Error saved Sender Name!'});
      }
    });
  }
  updateUsername(){
    let payload = {id:this.idusername, value:this.username};
    // console.log(JSON.stringify(payload));
    this.smtpconfigService.updateSmtpConfig(payload).subscribe((result:BackendResponse)=>{
      if(result.status===200){
        this.messageService.add({key: 'bc', severity:'success', summary: 'Success', detail: 'User Name saved!'});
      } else {
        this.messageService.add({key: 'bc', severity:'error', summary: 'Error', detail: 'Error saved User Name!'});
      }
    });
  }

  updatePassword(){
    let payload = {id:this.idpwdemail, value:this.pwdemail};
    // console.log(JSON.stringify(payload));
    this.smtpconfigService.updateSmtpConfig(payload).subscribe((result:BackendResponse)=>{
      if(result.status===200){
        this.messageService.add({key: 'bc', severity:'success', summary: 'Success', detail: 'Password saved!'});
      } else {
        this.messageService.add({key: 'bc', severity:'error', summary: 'Error', detail: 'Error saved Password!'});
      }
    });
  }

  updateSmtp(){
    let payload = {id:this.idsmtphost, value:this.smtphost};
    // console.log(JSON.stringify(payload));
    this.smtpconfigService.updateSmtpConfig(payload).subscribe((result:BackendResponse)=>{
      if(result.status===200){
        this.messageService.add({key: 'bc', severity:'success', summary: 'Success', detail: 'SMTP Server saved!'});
      } else {
        this.messageService.add({key: 'bc', severity:'error', summary: 'Error', detail: 'Error saved SMTP Server!'});
      }
    });
  }
  updatePort(){
    let payload = {id:this.idport, value:this.idport};
    // console.log(JSON.stringify(payload));
    this.smtpconfigService.updateSmtpConfig(payload).subscribe((result:BackendResponse)=>{
      if(result.status===200){
        this.messageService.add({key: 'bc', severity:'success', summary: 'Success', detail: 'Port Server saved!'});
      } else {
        this.messageService.add({key: 'bc', severity:'error', summary: 'Error', detail: 'Error saved Port Server!'});
      }
    });
  }
  onChange(event) {
    // console.log('event :' + event);
    // console.log(event.value);
    let payload = {id:this.idsecure, value:event.value+""};
    this.smtpconfigService.updateSmtpConfig(payload).subscribe((result:BackendResponse)=>{
      if(result.status===200){
        this.messageService.add({key: 'bc', severity:'success', summary: 'Success', detail: 'Secure server saved!'});
      } else {
        this.messageService.add({key: 'bc', severity:'error', summary: 'Error', detail: 'Error saved Secure Server!'});
      }
    });


  }

  showBottomCenter() {
    this.messageService.add({key: 'bc', severity:'success', summary: 'Success', detail: 'Message Content'});
}

}
