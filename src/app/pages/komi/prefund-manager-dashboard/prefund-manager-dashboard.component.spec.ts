import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrefundManagerDashboardComponent } from './prefund-manager-dashboard.component';

describe('PrefundManagerDashboardComponent', () => {
  let component: PrefundManagerDashboardComponent;
  let fixture: ComponentFixture<PrefundManagerDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrefundManagerDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrefundManagerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
