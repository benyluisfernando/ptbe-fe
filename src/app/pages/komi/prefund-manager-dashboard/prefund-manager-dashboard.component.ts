import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';
import { PrefundDashboardService } from 'src/app/services/komi/prefund-dashboard.service';
import { max } from 'moment';

@Component({
  selector: 'app-prefund-manager-dashboard',
  templateUrl: './prefund-manager-dashboard.component.html',
  styleUrls: ['./prefund-manager-dashboard.component.scss'],
})
export class PrefundManagerDashboardComponent implements OnInit {
  display = false;
  displayPrv = false;
  selectedProxy: any = [];
  breadcrumbs!: MenuItem[];
  isFetching: boolean = false;
  home!: MenuItem;

  userInfo: any = {};
  tokenID: string = '';
  limitData: any[] = [];
  prxheader: any = [
    { label: 'RefNo', sort: 'refNum' },
    { label: 'Transaction Type', sort: 'transactionType' },
    { label: 'Prefund Amount', sort: 'prefundAmount' },
    { label: 'datetime', sort: 'formated_date' },
  ];
  prxcolname: any = [
    'refNum',
    'transactionType',
    'prefundAmount',
    'formated_date',
  ];
  prxcolhalign: any = [
    'p-text-center',
    'p-text-center',
    'p-text-center',
    'p-text-center',
  ];
  prxcolwidth: any = ['', '', '', '', '', '', '', '', ''];
  prxcollinghref: any = { url: '#', label: 'Application' };
  limitactionbtn: any = [0, 0, 0, 0, 0];
  prxaddbtn = { route: 'detail', label: 'Add Data' };

  gaugeType = 'semi';
  gaugeValue;
  gaugeMax;
  gaugeMin;
  //gaugeAppendText = `/${this.gaugeMax}`;

  minRadar: 0;
  maxRadar: 1500;
  valueRadar: 1000;

  thresholdConfig = {
    minRadar: {color: 'red'},
    valueRadar: {color: 'green'},
    maxRadar: {color: 'orange'}
  };


  constructor(
    private authservice: AuthService,
    public dialogService: DialogService,
    public messageService: MessageService,
    private proxyadminService: ProxyadminService,
    private limitService: LimitService,
    private prefundDashboardService: PrefundDashboardService
  ) {}

  payload: any = {
    "transactionId": "000001",
    "dateTime": "2021-10-21T11:00:00.000",
    "merchantType": "6018",
    "terminalId": "MBS       ",
    "noRef": "20200420471644500911",
    "accountNumber": "1112223"
  };

  ngOnInit() {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [{ label: 'Prefund Managament' }];
    this.authservice.whoAmi().subscribe((value) => {
      // console.log(">>> User Info : "+JSON.stringify(value));
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
    });
    this.refreshingProxy();
    this.getProp();
    this.getBalanceInquiry();
  }

  getProp() {
    this.prefundDashboardService
      .getDashboardProp()
      .subscribe((result: BackendResponse) => {
        this.gaugeMax = result.data.dashboardProp.max;
        this.gaugeMin = result.data.dashboardProp.min;
       // this.gaugeValue = result.data.dashboardProp.current;
        //this.gaugeAppendText = `/${this.gaugeMax}`;

        console.log(result.data);
      });
  }
  refreshingProxy(): void {
    this.isFetching = true;
    // this.authservice.whoAmi().subscribe((data: BackendResponse) => {
    console.log('>>>>>>> Refresh Limit ');
    this.prefundDashboardService
      .getHistory()
      .subscribe(async (result: BackendResponse) => {
        console.log('>>>>>>> ' + JSON.stringify(result));
        if (result.status === 202) {
          this.limitData = [];

          let objtmp = {
            refNum: 'No records',
            transactionType: 'No records',
            prefundAmount: 'No records',
            formated_date: 'No records',
          };
          this.limitactionbtn = [1, 0, 0, 0, 0];
          this.limitData.push(objtmp);
        } else {
          this.limitData = result.data.historyData;
        }
        // if(result.data.userGroup.length > 0) {
        //   this.proxyData = [];
        //   this.proxyData = result.data.userGroup;
        // } else {
        //   this.proxyData = [];
        //   let objtmp = {"groupname":"No records"};
        //   this.proxyData.push(objtmp);
        // }
        this.isFetching = false;
      });
  }
  deleteConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.display = true;
    this.selectedProxy = data;
  }

  previewConfirmation(data: any) {
    console.log('Di Emit nih dari child ' + JSON.stringify(data));
    this.displayPrv = true;
    this.selectedProxy = data;
  }

  deleteLimit() {
    console.log(this.selectedProxy);
    this.limitService
      .delete(this.selectedProxy.id)
      .subscribe((resp: BackendResponse) => {
        this.display = false;
        this.showTopSuccess(resp.data);
        this.refreshingProxy();
      });
    // this.proxyadminService.deleteGroup(payload).subscribe((resp: BackendResponse)=>{
    //   console.log(">> hasil Delete "+ resp);
    //       if (resp.status === 200) {
    //       this.showTopSuccess(resp.data);
    //     }
    //   this.display = false;
    //   this.refreshingBic();
    // });
  }
  showTopSuccess(message: string) {
    this.messageService.add({
      severity: 'success',
      summary: 'Deleted',
      detail: message,
    });
  }

  getBalanceInquiry () {
    this.prefundDashboardService
      .balanceInquiry(this.payload)
      .subscribe((result: any) => {
        this.gaugeValue = result.balance;

        console.log(result.balance);
      });
  }
}
