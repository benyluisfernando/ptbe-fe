import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuItem, SelectItem } from 'primeng/api';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { ProxyadminService } from 'src/app/services/komi/proxyadmin.service';
import { BussinesparamService } from 'src/app/services/bussinesparam.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { BicadminService } from 'src/app/services/komi/bicadmin.service';
import { LimitService } from 'src/app/services/komi/limit.service';
import { ResidentstatusService } from 'src/app/services/komi/residentstatus.service';
import { PrefundDashboardService } from 'src/app/services/komi/prefund-dashboard.service';
@Component({
  selector: 'app-prefund-manage-dashboard-detail',
  templateUrl: './prefund-manage-dashboard-detail.component.html',
  styleUrls: ['./prefund-manage-dashboard-detail.component.scss'],
})
export class PrefundManageDashboardDetailComponent implements OnInit {
  extraInfo: any = {};
  isEdit: boolean = false;
  userId: any = null;
  stateOptions: any[] = [];
  breadcrumbs!: MenuItem[];
  home!: MenuItem;
  leveltenant: Number = 0;
  userInfo: any = {};
  selectedApps: any = [];
  proxytypes: any = [];
  proxystates: any = [];
  registrarbics: any = [];
  idtypes: any = [];
  tokenID: string = '';
  groupForm!: FormGroup;
  items: SelectItem[];
  item: string;
  submitted = false;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authservice: AuthService,
    private location: Location,
    private proxyadminService: ProxyadminService,
    private bussinesparamService: BussinesparamService,
    private bicadminService: BicadminService,
    private prefundDashboardService: PrefundDashboardService
  ) {
    this.extraInfo = this.router.getCurrentNavigation()?.finalUrl!.toString();
    let checkurl = this.extraInfo.indexOf('%23') !== -1 ? true : false;
    console.log('>>>>>>>>>>> ' + this.extraInfo);
    console.log(checkurl);
    if (checkurl) this.isEdit = true;
  }

  ngOnInit(): void {
    this.home = { icon: 'pi pi-home', routerLink: '/mgm/home' };
    this.breadcrumbs = [
      // {
      //   label: 'Resident Status',
      //   command: (event) => {
      //     this.location.back();
      //   },
      //   url: '',
      // },
      { label: !this.isEdit ? 'Edit data' : 'Add data' },
    ];
    this.authservice.whoAmi().subscribe((value) => {
      this.userInfo = value.data;
      this.tokenID = value.tokenId;
      this.leveltenant = this.userInfo.leveltenant;
      // registration_id, proxy_type, proxy_value, registrar_bic, account_number, account_type, account_name, identification_number_type, identification_number_value, proxy_status
      this.groupForm = this.formBuilder.group({
        min: ['', Validators.required],
        max: ['', Validators.required],
      });

      this.proxytypes = [];
      this.idtypes = [];
      this.registrarbics = [];
      this.bussinesparamService
        .getAllByCategory('KOMI_LMT_STS')
        .subscribe((result: BackendResponse) => {
          console.log('>>>>>>> ' + JSON.stringify(result));
          if (result.status === 200) {
            this.proxytypes = result.data;
          } else {
            // this.proxyData = result.data.bicAdmins;
          }
        });

      this.prefundDashboardService
        .getById(1)
        .subscribe(async (result: BackendResponse) => {
          console.log('Data edit bic ' + JSON.stringify(result.data));
          // if (this.isOrganization) {
          this.groupForm.patchValue({
            min: result.data.dashboardProp.min,
            max: result.data.dashboardProp.max,
          });
          // console.log(this.user);
        });
    });
  }
  get f() {
    return this.groupForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    // console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
    // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
    if (this.groupForm.valid) {
      // event?.preventDefault;
      // var groupacl = this.groupForm.get('orgobj')?.value;
      let payload = {};
      // bank_code, bic_code, bank_name

      payload = {
        id: 1,
        min: this.groupForm.get('min')?.value,
        max: this.groupForm.get('max')?.value,
      };
      console.log('>>>>>>>> payload ' + JSON.stringify(payload));
      this.prefundDashboardService
        .update(payload)
        .subscribe((result: BackendResponse) => {
          if (result.status == 200) {
            this.location.back();
          }
          console.log('>>>>>>>> payload ' + JSON.stringify(result));
          //this.location.back();
        });
    }

    console.log(this.groupForm.valid);
  }
}
