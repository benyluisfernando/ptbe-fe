import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { BackendService } from 'src/app/services/backend.service';
import { BackendResponse } from 'src/app/interfaces/backend-response';
import { AuthService } from 'src/app/services/auth.service';
import { Location } from '@angular/common';
import { ForgotpasswordService } from 'src/app/services/forgotpassword/forgotpassword.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss'],
})
export class ForgotpasswordComponent implements OnInit {
  blockedDocument: boolean = false;
  //  userid = "";
  secret = '';
  errorMsg = '';
  isProcess: boolean = false;
  userForm!: FormGroup;
  submitted = false;
  constructor(
    private messageService: MessageService,
    private sessionStorage: SessionStorageService,
    private route: Router,
    private formBuilder: FormBuilder,
    private backend: BackendService,
    private authservice: AuthService,
    private forgotpasswordService: ForgotpasswordService,
    private location: Location
  ) {}

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      emailid: ['', Validators.required],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.userForm.controls;
  }
  get userFormControl() {
    return this.userForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    const payload = {
      email: this.userForm.value['emailid'],
    };

    console.log('payload', payload);
    this.forgotpasswordService
      .forgot(payload)
      .subscribe((resp: BackendResponse) => {
        console.log(resp);
        if (resp.status === 200) {
          this.showTopCenterInfo('The mail has beed send');
          setTimeout(() => {
            this.route.navigate(['/auth/login']);
          }, 4000);
        } else if (resp.status === 201) {
          this.showTopCenterErr('Email is not found');
          setTimeout(() => {
            this.route.navigate(['/auth/login']);
          }, 4000);
        }
      });
  }

  showTopCenterInfo(message: string) {
    this.messageService.add({
      severity: 'info',
      summary: 'Confirmed',
      detail: message,
    });
  }

  showTopCenterErr(message: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: message,
    });
  }

  blockDocument() {
    this.blockedDocument = true;
    //  setTimeout(() => {
    //      this.blockedDocument = false;
    //      this.showTopCenterErr("Invalid user and password!")
    //  }, 3000);
  }
}
