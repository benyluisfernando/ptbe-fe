import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceusageComponent } from './resourceusage.component';

describe('ResourceusageComponent', () => {
  let component: ResourceusageComponent;
  let fixture: ComponentFixture<ResourceusageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResourceusageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourceusageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
