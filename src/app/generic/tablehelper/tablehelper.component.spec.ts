import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablehelperComponent } from './tablehelper.component';

describe('TablehelperComponent', () => {
  let component: TablehelperComponent;
  let fixture: ComponentFixture<TablehelperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablehelperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablehelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
