import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';

@Injectable({
  providedIn: 'root'
})
export class BussinesparamService {

  constructor(private service: BackendService) { }
  getAllBpByTenant() {
    const url = 'integrate/bp/bpla';
    return this.service.get(url);
  }
  getAllByCategory(payload: any) {
    const url = 'integrate/bp/bpbycategory/'+payload;
    return this.service.get(url);
  }
}
