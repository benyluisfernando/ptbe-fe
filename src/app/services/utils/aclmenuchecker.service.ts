import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AclmenucheckerService {
  allAcl: any = [];
  constructor() { }

  getAllMenus(){
    return this.allAcl;
  }
  async getAclMenu1(rlink) {
    console.log(">>>>>>>>> "+rlink)
    await this.allAcl.find(obj => {
      // console.log(JSON.stringify(obj));
      return obj;
    })
  }

  getAclMenu = async (rlink) => {
    return await this.allAcl.find(obj => {
      // console.log(JSON.stringify(obj));
      return obj;
    })
  }

  // getAclMenu = async (sidesmenu) => {
  //   this.allAcl = []
  //   return Promise.all(sidesmenu.map(item => this.doSomethingAsync(item)))
  // }

  setAllMenus = async (sidesmenu) => {
    this.allAcl = []
    return Promise.all(sidesmenu.map(item => this.doSomethingAsync(item)))
  }
  doSomethingAsync = async item => {
    return this.functionPopulateACL(item)
  }
  functionPopulateACL = item => {
    // console.log(item.items);
    let menupermodule =item.items;
    menupermodule.map(minimenu => {
      let obj = {routelink:minimenu.routerLink,acl:minimenu.acl}
      this.allAcl.push(obj);
    })
    // this.allAcl.push(item.items);
    return Promise.resolve('ok')
  }
}
