import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class ForgotpasswordService {
  constructor(private service: BackendService) {}

  forgot(payload: any) {
    const url = 'adm/send/forgotpassword';
    return this.service.post(url, payload);
  }

  reset(payload: any) {
    const url = `adm/send/resetpassword/${payload.id}`;
    return this.service.post(url, payload);
  }

  verifikasi(payload: any) {
    const url = 'adm/send/verifikasi';
    return this.service.post(url, payload);
  }

  verifikasiemail(payload: any) {
    const url = `adm/send/verifikasiemail/${payload.id}`;
    return this.service.post(url, payload);
  }
}
