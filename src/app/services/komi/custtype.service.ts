import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class CusttypeService {
  constructor(private service: BackendService) {}
  getAll() {
    const url = 'komi/custtype/getAll';
    return this.service.baseGet(url);
  }
  getById(payload: any) {
    const url = 'komi/custtype/getById/' + payload;
    return this.service.baseGet(url);
  }
  insert(payload: any) {
    const url = 'komi/custtype/insert';
    return this.service.basePost(url, payload);
  }
  update(payload: any) {
    const url = 'komi/custtype/update';
    return this.service.basePost(url, payload);
  }
  delete(payload: any) {
    const url = 'komi/custtype/delete/' + payload;
    return this.service.baseGet(url);
  }
}
