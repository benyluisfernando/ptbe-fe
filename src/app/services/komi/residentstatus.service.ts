import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class ResidentstatusService {
  constructor(private service: BackendService) {}
  getAll() {
    const url = 'komi/resident/getAll';
    return this.service.baseGet(url);
  }
  getById(payload: any) {
    const url = 'komi/resident/getById/' + payload;
    return this.service.baseGet(url);
  }
  insert(payload: any) {
    const url = 'komi/resident/insert';
    return this.service.basePost(url, payload);
  }
  update(payload: any) {
    const url = 'komi/resident/update';
    return this.service.basePost(url, payload);
  }
  delete(payload: any) {
    const url = 'komi/resident/delete/' + payload;
    return this.service.baseGet(url);
  }
}
