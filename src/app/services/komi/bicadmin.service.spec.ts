import { TestBed } from '@angular/core/testing';

import { BicadminService } from './bicadmin.service';

describe('BicadminService', () => {
  let service: BicadminService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BicadminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
