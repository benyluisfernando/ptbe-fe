import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class BicadminService {

  constructor(private service: BackendService) { }
  getAllBicByTenant() {
    const url = 'komi/bic/getAllbic';
    return this.service.baseGet(url);
  }
  getBicById(payload: any) {
    const url = 'komi/bic/getBic/'+payload;
    return this.service.baseGet(url);
  }
  insertBicByTenant(payload: any) {
    const url = 'komi/bic/insertBic';
    return this.service.basePost(url, payload);
  }
  updateBicByTenant(payload: any) {
    const url = 'komi/bic/updateBic';
    return this.service.basePost(url, payload);
  }
  deleteBicByTenant(payload: any) {
    const url = 'komi/bic/deleteBic/'+payload;
    return this.service.baseGet(url);
  }
}
