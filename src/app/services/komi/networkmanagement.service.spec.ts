import { TestBed } from '@angular/core/testing';

import { NetworkmanagementService } from './networkmanagement.service';

describe('NetworkmanagementService', () => {
  let service: NetworkmanagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NetworkmanagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
