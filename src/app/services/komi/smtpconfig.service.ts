import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class SmtpconfigService {

  constructor(private service: BackendService) { }
  retriveSmtpConfig() {
    const url = 'adm/smtpset/smtpsettings';
    return this.service.get(url);
  }

  updateSmtpConfig(payload:any) {

    const url = 'adm/smtpset/smtpupdate';
    return this.service.post(url, payload);
  }
}
