import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class ChanneltypeService {

  constructor(private service: BackendService) { }
  getAllChnByTenant() {
    const url = 'komi/channeltype/getAllChannelType';
    return this.service.baseGet(url);
  }
  getChnById(payload: any) {
    const url = 'komi/channeltype/getChannelType/'+payload;
    return this.service.baseGet(url);
  }
  insertChnByTenant(payload: any) {
    const url = 'komi/channeltype/insertChannelType';
    return this.service.basePost(url, payload);
  }
  updateChnByTenant(payload: any) {
    const url = 'komi/channeltype/updateChannelType';
    return this.service.basePost(url, payload);
  }
  deleteChnByTenant(payload: any) {
    const url = 'komi/channeltype/deleteChannelType/'+payload;
    return this.service.baseGet(url);
  }
}
