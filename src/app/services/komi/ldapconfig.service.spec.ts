import { TestBed } from '@angular/core/testing';

import { LdapconfigService } from './ldapconfig.service';

describe('LdapconfigService', () => {
  let service: LdapconfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LdapconfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
