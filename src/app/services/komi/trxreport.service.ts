import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class TrxreportService {

  constructor(private service: BackendService) { }
  getAllReport() {
    const url = 'komi/reports/getAllGenerated';
    return this.service.baseGet(url);
  }
  generatReport(payload:any) {
    const url = 'komi/reports/generateReportBy';
    return this.service.basePost(url, payload);
  }

  downloadReport(payload:any) {
    const url = 'komi/reports/downloadByid/'+payload;
    return this.service.baseGet(url);
  }



}
