import { TestBed } from '@angular/core/testing';

import { ProxyadminService } from './proxyadmin.service';

describe('ProxyadminService', () => {
  let service: ProxyadminService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProxyadminService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
