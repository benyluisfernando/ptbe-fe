import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class BranchService {

  constructor(private service: BackendService) { }
  getAllBranchByTenant() {
    const url = 'komi/branch/getAllBranch';
    return this.service.baseGet(url);
  }
  getBranchById(payload: any) {
    const url = 'komi/branch/getBranch/'+payload;
    return this.service.baseGet(url);
  }
  insertBranchByTenant(payload: any) {
    const url = 'komi/branch/insertBranch';
    return this.service.basePost(url, payload);
  }
  updateBranchByTenant(payload: any) {
    const url = 'komi/branch/updateBranch';
    return this.service.basePost(url, payload);
  }
  deleteBranchByTenant(payload: any) {
    const url = 'komi/branch/deleteBranch/'+payload;
    return this.service.baseGet(url);
  }
}
