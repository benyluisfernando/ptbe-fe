import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class ProxytypeService {

  constructor(private service: BackendService) { }
  getAllPrxTByTenant() {
    const url = 'komi/proxytype/getAllProxyType';
    return this.service.baseGet(url);
  }
  getPrxTById(payload: any) {
    const url = 'komi/proxytype/getProxyType/'+payload;
    return this.service.baseGet(url);
  }
  insertPrxTByTenant(payload: any) {
    const url = 'komi/proxytype/insertProxyType';
    return this.service.basePost(url, payload);
  }
  updatePrxTByTenant(payload: any) {
    const url = 'komi/proxytype/updateProxyType';
    return this.service.basePost(url, payload);
  }
  deletePrxTByTenant(payload: any) {
    const url = 'komi/proxytype/deleteProxyType/'+payload;
    return this.service.baseGet(url);
  }
}
