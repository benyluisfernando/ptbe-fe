import { TestBed } from '@angular/core/testing';

import { TrxreportService } from './trxreport.service';

describe('TrxreportService', () => {
  let service: TrxreportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrxreportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
