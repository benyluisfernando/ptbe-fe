import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class SysparamService {

  constructor(private service: BackendService) { }
  getAllSysParByTenant() {
    const url = 'komi/sysparam/getAllSysParam';
    return this.service.baseGet(url);
  }
  getSysParById(payload: any) {
    const url = 'komi/sysparam/getSysParam/'+payload;
    return this.service.baseGet(url);
  }
  insertSysParByTenant(payload: any) {
    const url = 'komi/sysparam/insertSysParam';
    return this.service.basePost(url, payload);
  }
  updateSysParByTenant(payload: any) {
    const url = 'komi/sysparam/updateSysParam';
    return this.service.basePost(url, payload);
  }
  updateSysParByTenantActive(payload: any) {
    const url = 'komi/sysparam/updateSysParamActive';
    return this.service.basePost(url, payload);
  }
  deleteSysParByTenant(payload: any) {
    const url = 'komi/sysparam/deleteSysParam/'+payload;
    return this.service.baseGet(url);
  }
}
