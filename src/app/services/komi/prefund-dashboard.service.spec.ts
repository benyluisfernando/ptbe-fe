import { TestBed } from '@angular/core/testing';

import { PrefundDashboardService } from './prefund-dashboard.service';

describe('PrefundDashboardService', () => {
  let service: PrefundDashboardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrefundDashboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
