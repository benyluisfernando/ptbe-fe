import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root',
})
export class PrefundDashboardService {
  getById(id: any) {
    const url = 'komi/prefunddashboard/getProp';
    return this.service.baseGet(url);
  }
  constructor(private service: BackendService) {}
  getDashboardProp() {
    const url = 'komi/prefunddashboard/getProp';
    return this.service.baseGet(url);
  }
  getHistory() {
    const url = 'komi/prefunddashboard/getHistory/';
    return this.service.baseGet(url);
  }

  update(payload: any) {
    const url = 'komi/prefunddashboard/update';
    return this.service.basePost(url, payload);
  }

  balanceInquiry(payload: any) {
    const url = 'komi/prefunddashboard/external/balanceinquiry';
    return this.service.basePost(url, payload);
  }
}
