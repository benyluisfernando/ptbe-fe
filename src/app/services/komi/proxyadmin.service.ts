import { Injectable } from '@angular/core';
import { BackendService } from '../backend.service';

@Injectable({
  providedIn: 'root'
})
export class ProxyadminService {

  constructor(private service: BackendService) { }
  getAllProxyByTenant() {
    const url = 'komi/Proxy/getAllProxy';
    return this.service.baseGet(url);
  }
  getProxyByParam(payload: any) {
    const url = 'komi/Proxy/getProxyByParam';
    return this.service.basePost(url, payload);
  }
}
