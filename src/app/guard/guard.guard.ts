import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { Observable } from 'rxjs';
import { BackendResponse } from '../interfaces/backend-response';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class GuardGuard implements CanActivate {
  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    // console.log(route.params.config);
    let config = route.params.config;
    if (this.authService.isLoggedIn()) {
      //console.log(' sudah logged in');

      return await this.checkPage(this.authService, route, state, this.router);
    } else {
      if (route.params.config) {
        this.authService
          .whoAmiInitialize(route.params.config)
          .subscribe((value) => {
            console.log('>>> Token session : ' + JSON.stringify(value));
            if ((value.status = 200)) {
              this.sessionStorage.set('accesstoken', value.data);
              this.authService.setAuthStatus(true);
              this.authService.setToken(value.data);
              this.router.navigate(['mgm/home']);
            } else {
              this.router.navigate(['auth/login']);
            }
          });
      } else {
        this.router.navigate(['/auth/login']);
        return false;
      }
    }

    // return false;
  }
  constructor(
    private authService: AuthService,
    private router: Router,
    private sessionStorage: SessionStorageService
  ) {}

  async checkPage(
    authService: AuthService,
    routeCp: ActivatedRouteSnapshot,
    stateCp: RouterStateSnapshot,
    router: Router
  ): Promise<boolean> {
    const promise = new Promise(function (resolve, reject) {
      // setTimeout(function() {
      authService.whoAmi().subscribe((value: BackendResponse) => {
        resolve(value);
        // console.log(JSON.stringify(value.data));
        // let lvlTenant = parseInt(value.data.leveltenant);

        // if (lvlTenant > 0) {
        //   let anypage = false;
        //   value.data.sidemenus.forEach((module: any) => {
        //     module.items.forEach((menus: any) => {
        //       if (menus.routerLink) {
        //         let routerLink = menus.routerLink;
        //         let mainModule = routerLink.split('/');
        //         let module = mainModule[2];
        //         let currentPath = stateCp.url;

        //         if (
        //           currentPath.includes(module) ||
        //           currentPath.includes('profile')
        //         )
        //           anypage = true;
        //       }
        //     });
        //   });

        //   if (stateCp.url === '/home') anypage = true;
        //   //if (stateCp.url === '/vam') anypage = true;
        //   console.log('Ada Halaman ' + stateCp.url);
        //   if (anypage) {
        //     resolve('Promise returns after 1.5 second!');
        //   } else {
        //     router.navigate(['/noauth/err']);
        //     reject(anypage);
        //   }
        // } else {
        //   resolve('Promise returns after 1.5 second!');
        // }
      });
    });
    return promise.then(function (value: any) {
      // console.log(value);
      if (value.exp == true) {
        router.navigate(['/auth/login']);
        return false;
      }
      return true;
      // Promise returns after 1.5 second!
    });
  }
}
