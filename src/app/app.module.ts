import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {
  HashLocationStrategy,
  LocationStrategy,
  PathLocationStrategy,
} from '@angular/common';
import { AppComponent } from './app.component';
import { MessageService } from 'primeng/api';
import { CardModule } from 'primeng/card';
import { ToolbarModule } from 'primeng/toolbar';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { MenubarModule } from 'primeng/menubar';
import { PanelMenuModule } from 'primeng/panelmenu';
import { DialogModule } from 'primeng/dialog';
import { PanelModule } from 'primeng/panel';
import { CheckboxModule } from 'primeng/checkbox';
import {
  DynamicDialogModule,
  DialogService,
  DynamicDialogRef,
  DynamicDialogConfig,
} from 'primeng/dynamicdialog';
import { DividerModule } from 'primeng/divider';
import { MainmenulayoutComponent } from './layout/mainmenulayout/mainmenulayout.component';
import { NomenulayoutComponent } from './layout/nomenulayout/nomenulayout.component';
import { HomeadminComponent } from './pages/homeadmin/homeadmin.component';
import { HomeComponent } from './pages/home/home.component';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownModule } from 'primeng/dropdown';
import { BlockUIModule } from 'primeng/blockui';
import { SelectButtonModule } from 'primeng/selectbutton';
import { StepsModule } from 'primeng/steps';
import { InputNumberModule } from 'primeng/inputnumber';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ChartModule } from 'primeng/chart';
import { TableModule } from 'primeng/table';
import { PickListModule } from 'primeng/picklist';
import { TooltipModule } from 'primeng/tooltip';
import { ListboxModule } from 'primeng/listbox';
import { OrderListModule } from 'primeng/orderlist';
import { CalendarModule } from 'primeng/calendar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { NgxGaugeModule } from 'ngx-gauge';
import { ChipModule } from 'primeng/chip';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastModule } from 'primeng/toast';

import { ApplicationgroupComponent } from './pages/ptbc/applicationgroup/applicationgroup.component';
import { UsersComponent } from './pages/ptbc/users/users.component';
import { ApplicationsComponent } from './pages/root/applications/applications.component';
import { SmtpaccountsComponent } from './pages/root/smtpaccounts/smtpaccounts.component';
import { OauthsettingsComponent } from './pages/root/oauthsettings/oauthsettings.component';
import { EventlogsComponent } from './pages/root/eventlogs/eventlogs.component';
import { ResourceusageComponent } from './pages/root/resourceusage/resourceusage.component';
import { ErrorpageComponent } from './pages/errorpage/errorpage.component';
import { BackmenulayoutComponent } from './layout/backmenulayout/backmenulayout.component';
import { LoginComponent } from './pages/login/login.component';
import { ForgotpasswordComponent } from './pages/forgotpassword/forgotpassword.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorHttpService } from './interceptors/interceptor-http.service';
import { FullmenulayoutComponent } from './layout/fullmenulayout/fullmenulayout.component';
import { TablehelperComponent } from './generic/tablehelper/tablehelper.component';
import { ApplicationdetailComponent } from './pages/ptbc/applicationgroup/applicationdetail/applicationdetail.component';
import { UserdetailComponent } from './pages/ptbc/users/userdetail/userdetail.component';
import { BicadministrationComponent } from './pages/komi/bicadministration/bicadministration.component';
import { ProxymaintenanceComponent } from './pages/komi/proxymaintenance/proxymaintenance.component';
import { TransactionmonitorComponent } from './pages/komi/transactionmonitor/transactionmonitor.component';
import { TransactionreportComponent } from './pages/komi/transactionreport/transactionreport.component';
import { EnvServiceProvider } from './env/env.service.provider';
import { KomihomeComponent } from './pages/komi/komihome/komihome.component';
import { KomiinterceptComponent } from './pages/komi/komihome/komiintercept.component';
import { BicadmindetailComponent } from './pages/komi/bicadministration/bicadmindetail/bicadmindetail.component';
import { ProxymaintenancedetailComponent } from './pages/komi/proxymaintenance/proxymaintenancedetail/proxymaintenancedetail.component';
import { SystemparamComponent } from './pages/ptbc/systemparam/systemparam.component';
import { SystemparamdetailComponent } from './pages/ptbc/systemparam/systemparamdetail/systemparamdetail.component';
import { AliasproxyComponent } from './pages/komi/aliasproxy/aliasproxy.component';
import { AliasproxydetailComponent } from './pages/komi/aliasproxy/aliasproxydetail/aliasproxydetail.component';
import { NetworkmanageComponent } from './pages/komi/networkmanage/networkmanage.component';
import { ChanneltypeComponent } from './pages/komi/channeltype/channeltype.component';
import { ChanneltypedetailComponent } from './pages/komi/channeltype/channeltypedetail/channeltypedetail.component';
import { ProxytypeComponent } from './pages/komi/proxytype/proxytype.component';
import { ProxytypedetailComponent } from './pages/komi/proxytype/proxytypedetail/proxytypedetail.component';
import { PrefundmanagerComponent } from './pages/komi/prefundmanager/prefundmanager.component';
import { PrefundmanagerdetailComponent } from './pages/komi/prefundmanager/prefundmanagerdetail/prefundmanagerdetail.component';
import { TransactioncostComponent } from './pages/komi/transactioncost/transactioncost.component';
import { TransactioncostdetailComponent } from './pages/komi/transactioncost/transactioncostdetail/transactioncostdetail.component';
import { BranchComponent } from './pages/komi/branch/branch.component';
import { BranchdetailComponent } from './pages/komi/branch/branchdetail/branchdetail.component';
import { LimitComponent } from './pages/komi/limit/limit.component';
import { LimitdetailComponent } from './pages/komi/limit/limitdetail/limitdetail.component';
import { MappingresidentComponent } from './pages/komi/mappingresident/mappingresident.component';
import { MappingresidentdetailComponent } from './pages/komi/mappingresident/mappingresidentdetail/mappingresidentdetail.component';
import { MappingIdTypeComponent } from './pages/komi/mapping-id-type/mapping-id-type.component';
import { MappingIdTypeDetailComponent } from './pages/komi/mapping-id-type/mapping-id-type-detail/mapping-id-type-detail.component';
import { MappingAccountTypeComponent } from './pages/komi/mapping-account-type/mapping-account-type.component';
import { MappingAccountTypeDetailComponent } from './pages/komi/mapping-account-type/mapping-account-type-detail/mapping-account-type-detail.component';
import { MappingCustomerTypeComponent } from './pages/komi/mapping-customer-type/mapping-customer-type.component';
import { MappingCustomerTypeDetailComponent } from './pages/komi/mapping-customer-type/mapping-customer-type-detail/mapping-customer-type-detail.component';
import { PrefundManagerDashboardComponent } from './pages/komi/prefund-manager-dashboard/prefund-manager-dashboard.component';
import { PrefundManageDashboardDetailComponent } from './pages/komi/prefund-manager-dashboard/prefund-manage-dashboard-detail/prefund-manage-dashboard-detail.component';
import { DatePipe } from '@angular/common';
import { LogmonitorComponent } from './pages/komi/logmonitor/logmonitor.component';
import { ActionlogComponent } from './pages/komi/actionlog/actionlog.component';
import { SystemlogComponent } from './pages/komi/systemlog/systemlog.component';
import { EventLogComponent } from './pages/komi/event-log/event-log.component';
import { SysParamComponent } from './pages/komi/sys-param/sys-param.component';
import { SysParamDetailComponent } from './pages/komi/sys-param/sys-param-detail/sys-param-detail.component';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { ProfileComponent } from './pages/root/profile/profile.component';
import { ChangePasswordComponent } from './pages/root/change-password/change-password.component';
import { ResetpasswordComponent } from './pages/forgotpassword/resetpassword/resetpassword.component';
import { SmtpconfigComponent } from './pages/komi/smtpconfig/smtpconfig.component';
import { LdapconfigComponent } from './pages/komi/ldapconfig/ldapconfig.component';
import { AdminnotificationComponent } from './pages/komi/adminnotification/adminnotification.component';
import { AdminnotifdetailComponent } from './pages/komi/adminnotification/adminnotifdetail/adminnotifdetail.component';
import { MonthlyChartComponent } from './pages/komi/komihome/component/monthly-chart/monthly-chart.component';
import { DailyChartComponent } from './pages/komi/komihome/component/daily-chart/daily-chart.component';
import { WeeklyChartComponent } from './pages/komi/komihome/component/weekly-chart/weekly-chart.component';
import { VerifikasiemailComponent } from './pages/forgotpassword/verifikasiemail/verifikasiemail.component';
import { SystemLogInboundComponent } from './pages/komi/system-log-inbound/system-log-inbound.component';
import { SystemLogOutboundComponent } from './pages/komi/system-log-outbound/system-log-outbound.component';
import { PtbchomeComponent } from './pages/ptbc/ptbchome/ptbchome.component';
import { CompanyComponent } from './pages/ptbc/company/company.component';
import { CompanydetailComponent } from './pages/ptbc/company/companydetail/companydetail.component';
import { ChannelComponent } from './pages/ptbc/channel/channel.component';
import { ChanneldetailComponent } from './pages/ptbc/channel/channeldetail/channeldetail.component';
import { CompanyreqComponent } from './pages/ptbc/companyreq/companyreq.component';
import { CompanyreqdetailComponent } from './pages/ptbc/companyreq/companyreqdetail/companyreqdetail.component';
import { ChannelreqComponent } from './pages/ptbc/channelreq/channelreq.component';
import { ChannelreqdetailComponent } from './pages/ptbc/channelreq/channelreqdetail/channelreqdetail.component';
import { RulesreqComponent } from './pages/ptbc/rulesreq/rulesreq.component';
import { RulesreqdetailComponent } from './pages/ptbc/rulesreq/rulesreqdetail/rulesreqdetail.component';
import { RulesComponent } from './pages/ptbc/rules/rules.component';
import { RulesdetailComponent } from './pages/ptbc/rules/rulesdetail/rulesdetail.component';

@NgModule({
  declarations: [
    AppComponent,
    MainmenulayoutComponent,
    NomenulayoutComponent,
    HomeadminComponent,
    HomeComponent,
    ApplicationgroupComponent,
    UsersComponent,
    ApplicationsComponent,
    SmtpaccountsComponent,
    OauthsettingsComponent,
    EventlogsComponent,
    ResourceusageComponent,
    ErrorpageComponent,
    BackmenulayoutComponent,
    LoginComponent,
    ForgotpasswordComponent,
    FullmenulayoutComponent,
    TablehelperComponent,
    ApplicationdetailComponent,
    UserdetailComponent,
    BicadministrationComponent,
    ProxymaintenanceComponent,
    TransactionmonitorComponent,
    TransactionreportComponent,
    KomihomeComponent,
    KomiinterceptComponent,
    BicadmindetailComponent,
    ProxymaintenancedetailComponent,
    SystemparamComponent,
    SystemparamdetailComponent,
    AliasproxyComponent,
    AliasproxydetailComponent,
    NetworkmanageComponent,
    ChanneltypeComponent,
    ChanneltypedetailComponent,
    ProxytypeComponent,
    ProxytypedetailComponent,
    PrefundmanagerComponent,
    PrefundmanagerdetailComponent,
    TransactioncostComponent,
    TransactioncostdetailComponent,
    BranchComponent,
    BranchdetailComponent,
    LimitComponent,
    LimitdetailComponent,
    MappingresidentComponent,
    MappingresidentdetailComponent,
    MappingIdTypeComponent,
    MappingIdTypeDetailComponent,
    MappingAccountTypeComponent,
    MappingAccountTypeDetailComponent,
    MappingCustomerTypeComponent,
    MappingCustomerTypeDetailComponent,
    PrefundManagerDashboardComponent,
    PrefundManageDashboardDetailComponent,
    LogmonitorComponent,
    ActionlogComponent,
    SystemlogComponent,
    EventLogComponent,
    SysParamComponent,
    SysParamDetailComponent,
    ProfileComponent,
    ChangePasswordComponent,
    ResetpasswordComponent,
    SmtpconfigComponent,
    LdapconfigComponent,
    AdminnotificationComponent,
    AdminnotifdetailComponent,
    MonthlyChartComponent,
    DailyChartComponent,
    WeeklyChartComponent,
    VerifikasiemailComponent,
    SystemLogInboundComponent,
    SystemLogOutboundComponent,
    PtbchomeComponent,
    CompanyComponent,
    CompanydetailComponent,
    ChannelComponent,
    ChanneldetailComponent,
    CompanyreqComponent,
    CompanyreqdetailComponent,
    ChannelreqComponent,
    ChannelreqdetailComponent,
    RulesreqComponent,
    RulesreqdetailComponent,
    RulesComponent,
    RulesdetailComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    ToolbarModule,
    ButtonModule,
    InputTextModule,
    SplitButtonModule,
    MenubarModule,
    PanelMenuModule,
    BreadcrumbModule,
    DropdownModule,
    ChartModule,
    BlockUIModule,
    DividerModule,
    ProgressSpinnerModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    DialogModule,
    DynamicDialogModule,
    PanelModule,
    TableModule,
    SelectButtonModule,
    StepsModule,
    InputNumberModule,
    AutoCompleteModule,
    PickListModule,
    ListboxModule,
    OrderListModule,
    CheckboxModule,
    CalendarModule,
    ChipModule,
    NgxGaugeModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    TooltipModule,
  ],

  // import { SelectButtonModule } from 'primeng/selectbutton';
  // import { StepsModule } from 'primeng/steps';
  // import { InputNumberModule } from 'primeng/inputnumber';
  // import { AutoCompleteModule } from 'primeng/autocomplete';

  providers: [
    EnvServiceProvider,
    DynamicDialogRef,
    DynamicDialogConfig,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorHttpService,
      multi: true,
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    DialogService,
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
