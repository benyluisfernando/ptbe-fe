import { EnvService } from './env.service'; // import environment service

export const EnvServiceFactory = () => {  
  const env: any = new EnvService(); // Create env Instance

  // Read environment variables from browser window
  const browserWindow: any = window || {};
  const browserWindowEnv = browserWindow['env']|| {};

  // Assign environment variables from browser window to env
  // In the current implementation, properties from env.js overwrite defaults from the EnvService.
  // If needed, a deep merge can be performed here to merge properties instead of overwriting them.
  for (const key in browserWindowEnv) {
    // console.log(">>>>>>>>>>>>>> KEY S "+ key);
    if (browserWindowEnv.hasOwnProperty(key)) {
      env[key] = (window as any)['env'][key];
    }
  }

  return env;
};

export const EnvServiceProvider = {  
  provide: EnvService,
  useFactory: EnvServiceFactory,
  deps: [],
};

